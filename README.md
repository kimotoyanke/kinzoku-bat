# kinzoku-bat

## kinzoku-bat とは？

S 式系 AltJS です。

単純なトランスパイルルールですが、コードを動的に変更する機能を持ち、主にフレームワーク上でのプログラミングで、高速かつ快適に開発を進めることができるように設計されています。通常、JIS キーボードで Shift キーを押さずにプログラミングできるはずです。

## kbconfig.js の作成

kbconfig.js はサーバーのブートストラップとなる部分です。

```
// @ts-check
const { Service } = require('./dist/index')
// 使用するプラグインの呼び出し
const core = require('kb-plugin-core')
const express = require('kb-plugin-express')

// 使用するプラグインの呼び出し
const config = new Service([core.default, express.default])

module.exports = config
```

## サーバーの起動

`kbconfig.js`を作成し、`kbcli`で起動します。

## 変換

### 実行可能リスト

リストは普通、関数に変換されます。ただし、開き括弧と最初の要素の間の空白類はあってはなりません。

kinzoku-bat では、`()`、`[]`、`{}`の三つの括弧が使えます。  
括弧は、すべて同様に処理されます。

```lisp
(console.log "Hello, World")
[console.log "Hello, World with square bracket!"]
{console.log "Hello, World with curly bracket!"}
```

```javascript
console.log('Hello, World')
console.log('Hello, World with square bracket!')
console.log('Hello, World with curly bracket!')
```

```lisp
[ console.log "Hello, World" ]
```

### 実行不可能リスト

S 式の開き括弧と最初の要素の間に空白類があると、配列として処理されます。

```lisp
( console.log "If there's spaces between beginning bracket and first element, convert as a array")
```

```javascript
;[
  console.log,
  "If there's spaces between beginning bracket and first element, convert as a array"
]
```

### セミコロンセパレーター

リストの内部でセミコロンを挟むと、そのセミコロンで分割されて入れ子リストになります。

実行可能リストの場合実行可能リストに、実行不可能リストの場合実行不可能リストになります。

```lisp
[
1 2 3;
4 5 6;
7 8 9;
]
(console.log + 1 2; - 2 1;)
```

```javascript
;[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
console.log(1 + 2, 2 - 1)
```

### ハイフンキャピタル

ケバブケース(`kebab-case`)でキャメルケース(`camelCase`)の命名を記述するために、`-[a-z]`を`[A-Z]`に変換する機能があります。

```lisp
[console.log [window.to-string]]
```

```javascript
console.log(window.toString())
```

## 即時展開マクロ

[akameco/s2s](https://github.com/akameco/s2s) を参考にした即時展開マクロの機能を持ちます。

具体的にはパッケージの作成が必要です。package/plugin-express を参照してください。

### ボイラープレート

特定のフォルダに特定の形式の名前のファイルを作成すると、ファイルにボイラープレートを展開します。

```lisp
(importFrom [
  BoilerplatePluginDefinable
  Plugin
] "kinzoku-bat")
(export (const routes (new
  BoilerplatePluginDefinable
  "Routes"
  [ "routes/**.kbjs" ]
  {λ [ filename ]
    (return `{do
      (importFrom express "express")
      (const router (express.Router ))
      (router.get "/" {λ [ req res next ]
        (res.render "index" [ title: "Express"; ])
      })
      (exportDefault router)
    })
  }
)))
(exportDefault (new Plugin "express" [ routes ]))
```

とプラグイン側で指定すると、

`routes/**.kbjs`に新ファイルができたとき、

```list
(importFrom express "express")
(const router (express.Router ))
(router.get "/" {λ [ req res next ]
  (res.render "index" [ title: "Express"; ])
})
(exportDefault router)
```

というボイラープレートを展開します。

quasiquote 機能と do 文により、簡単に記述することができます。

### 即時展開インライン関数

特定の関数をインライン関数として予め指定することで、難しい記述を一瞬で書くことができます。

```
(importFrom [
  InlineFunctionPluginDefinable
  Plugin
] "kinzoku-bat")
(export (const rest (new
  InlineFunctionPluginDefinable
  "REST"
  "rest"
  {λ [ env router ]
    (return `{do
      (,router.get "/" {λ [ req res next ]
        (res.send "get")
      })
      (,router.post "/" {λ [ req res next ]
        (res.send "post")
      })
      (,router.put "/" {λ [ req res next ]
        (res.send "put")
      })
      (,router.delete "/" {λ [ req res next ]
        (res.send "delete")
      })
    })
  }
)))

(exportDefault (new Plugin "express" [ rest ]))
```

とプラグイン側で指定すると、

```
(rest routerA)
```

という関数がコードにあったとき、

```
(routerA.get "/" {λ [ req res next ]
  (res.send "get")
})
(routerA.post "/" {λ [ req res next ]
  (res.send "post")
})
(routerA.put "/" {λ [ req res next ]
  (res.send "put")
})
(routerA.delete "/" {λ [ req res next ]
  (res.send "delete")
})
```

という処理で置換されます。
