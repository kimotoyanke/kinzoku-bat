import * as t from '@babel/types'
import { parseSExp } from '../../lib/parser/parse'
import { transpileDefault } from '../../lib/transpiler/transpile'

describe('statement', function() {
  describe('core', function() {
    it('for statement', function() {
      const forExpression = transpileDefault(
        parseSExp('(for let i 0;< i 10;inc i;{do })')[0]
      )
      expect(t.isForStatement(forExpression)).toBeTruthy()
    })
    it('if statement', function() {
      const ifStatement = transpileDefault(parseSExp('(if == i 0; {do a})')[0])
      expect(t.isIfStatement(ifStatement)).toBeTruthy()
    })
    it('if-else statement', function() {
      const ifStatement = transpileDefault(
        parseSExp('(if == i 0; {do a} {else b})')[0]
      ) as t.IfStatement
      expect(t.isIfStatement(ifStatement)).toBeTruthy()
      expect(ifStatement.alternate).toBeDefined()
    })

    it('do statement', function() {
      const blockExpression = transpileDefault(
        parseSExp('{do (console.log "a")}')[0]
      )
      expect(t.isBlockStatement(blockExpression)).toBeTruthy()
    })
    it('lambda statement', function() {
      const lambdaExpression = transpileDefault(
        parseSExp('{lambda [a] (console.log a)}')[0]
      )
      expect(t.isArrowFunctionExpression(lambdaExpression)).toBeTruthy()
    })
  })
  describe('modules', function() {
    it('import from statement', function() {
      const importFromExpression = transpileDefault(
        parseSExp('[import-from a "b"]')[0]
      ) as t.ImportDeclaration
      expect(t.isImportDeclaration(importFromExpression)).toBeTruthy()
      expect(importFromExpression.source.value).toStrictEqual('b')
    })

    it('default export statement', function() {
      const exportExpression = transpileDefault(
        parseSExp('[export Exported]')[0]
      )
      expect(t.isExportDefaultDeclaration(exportExpression)).toBeTruthy()
    })

    it('named export statement', function() {
      const exportExpression = transpileDefault(
        parseSExp('[export [let a b]]')[0]
      )
      expect(t.isExportNamedDeclaration(exportExpression)).toBeTruthy()
    })
  })
})
