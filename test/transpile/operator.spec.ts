import * as t from '@babel/types'
import { parseSExp } from '../../lib/parser/parse'
import { transpileDefault } from '../../lib/transpiler/transpile'

describe('operator', function() {
  it('binary operators', function() {
    const opExpression = transpileDefault(parseSExp('(+ 1 2)')[0])
    expect(t.isBinaryExpression(opExpression)).toBeTruthy()
  })
  it('get-property', function() {
    const getPropExpression = transpileDefault(
      parseSExp('(get-property 5 [ a b c ])')[0]
    ) as t.MemberExpression
    expect(t.isMemberExpression(getPropExpression)).toBeTruthy()
    expect(t.isNumericLiteral(getPropExpression.property)).toBeTruthy()
  })
  it('spread element', function() {
    const arrayWhichHasSpreadElement = transpileDefault(
      parseSExp('[ 1 2 3 ...a ]')[0]
    ) as t.ArrayExpression
    expect(t.isArrayExpression(arrayWhichHasSpreadElement)).toBeTruthy()
    expect(
      t.isSpreadElement(arrayWhichHasSpreadElement.elements.reverse()[0])
    ).toBeTruthy()
  })
})
