import generate from '@babel/generator'
import * as t from '@babel/types'
import { parseSExp } from '../../lib/parser/parse'
import { SNumeric, SString } from '../../lib/s-ast/s-literal'
import {
  mapTranspileDefault,
  transpileDefault
} from '../../lib/transpiler/transpile'

describe('literal', function() {
  it('transpile function', function() {
    expect(
      generate(mapTranspileDefault(parseSExp('(console.log a)'))[0]).code
    ).toEqual('console.log(a)')
  })
  it('transpile string', function() {
    const val = 'a'
    const stringLiteral = transpileDefault(new SString(val)) as t.StringLiteral
    expect(stringLiteral.type).toBe('StringLiteral')
    expect(stringLiteral.value).toBe(val)
  })
  it('transpile numeral', function() {
    const val = 123
    const stringLiteral = transpileDefault(
      new SNumeric(123)
    ) as t.NumericLiteral
    expect(stringLiteral.type).toBe('NumericLiteral')
    expect(stringLiteral.value).toBe(val)
  })
  it('transpile object', function() {
    const objectLiteral = transpileDefault(
      parseSExp('( a: b; )')[0]
    ) as t.ObjectExpression
    expect(objectLiteral.type).toBe('ObjectExpression')
    expect(
      ((objectLiteral.properties[0] as t.ObjectProperty).key as t.Identifier)
        .name
    ).toBe('a')
  })
  it('transpile object 2', function() {
    const callExpression = transpileDefault(
      parseSExp(`
        (res.render "index" [ title: "Express"; ] )
      `)[0]
    ) as t.CallExpression
    expect(callExpression.type).toBe('CallExpression')
    expect(
      (((callExpression.arguments[1] as t.ObjectExpression)
        .properties[0] as t.ObjectProperty).key as t.Identifier).name
    ).toBe('title')
    expect(
      (((callExpression.arguments[1] as t.ObjectExpression)
        .properties[0] as t.ObjectProperty).value as t.StringLiteral).value
    ).toBe('Express')
  })
})
