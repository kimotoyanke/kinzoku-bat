import * as t from '@babel/types'
import { parseSExp } from '../../lib/parser/parse'
import { mapTranspileDefault } from '../../lib/transpiler/transpile'

describe('transpile macros', function() {
  it('list macro', function() {
    const [importStatement, newExpression] = mapTranspileDefault(
      parseSExp('(list a b c)')
    )
    expect(t.isImport(importStatement))
    expect(
      (((importStatement as t.ImportDeclaration)
        .specifiers[0] as t.ImportSpecifier).imported as t.Identifier).name
    ).toEqual('_SList')
    expect(t.isNewExpression(newExpression))
  })
})
