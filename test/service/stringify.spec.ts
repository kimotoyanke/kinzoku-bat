import { curry } from 'ramda'
import { parseSExp } from '../../lib/parser/parse'
import { separatorListTypes } from '../../lib/s-ast/list-type'
import { SIdentifier } from '../../lib/s-ast/s-identifier'
import { SList } from '../../lib/s-ast/s-list'
import { Service, defaultService } from '../../lib/service'
import { getMacroPluginDefinables } from '../../lib/service/config'
import { MacroPluginDefinable, Plugin } from '../../lib/service/plugin'
import { reindentSExp, reindentSProgram } from '../../lib/service/reindent'
import { Environment } from '../../lib/transpiler/env'

const reindent = curry((service: Service, code: string) =>
  reindentSProgram(service, parseSExp(code))
)
const reindentDefault = reindent(defaultService)

describe('reindent', function() {
  it('single col', function() {
    expect(
      reindentDefault('[console.log                             a]')
    ).toStrictEqual('(console.log a)')
  })
  it('semi', function() {
    const sexp: SList = new SList(
      [new SIdentifier('inc'), new SIdentifier('i')],
      separatorListTypes['semi']
    )
    expect(reindentSExp(defaultService, sexp)).toStrictEqual(`inc i;`.trim())
  })

  it('tab rule', function() {
    const sexp: SList = parseSExp(
      '[for let i 0; < i 100; [inc i] (console.log i) (console.log (+ i 2))]'
    )[0] as SList
    sexp.statementOption = 3
    const service = new Service([])
    service.indent = 'tab'
    expect(service.stringify([sexp])).toStrictEqual(
      `
[for let i 0; < i 100; (inc i)
\t(console.log i)
\t(console.log (+ i 2))
]
      `.trim()
    )
  })
  it('unexecutable list', function() {
    expect(reindentDefault('[ 1 2 3; 4 5 6; 7 8    9;]')).toStrictEqual(
      '[ 1 2 3; 4 5 6; 7 8 9; ]'
    )
  })
  it('get macro plugin definables from a plugin', function() {
    const plugin = new Plugin('plugin-identity', [
      new MacroPluginDefinable('idenitity', (env, a) => a)
    ])
    const definables = getMacroPluginDefinables([plugin])
    expect(definables).toHaveLength(1)
  })
})

describe('config', function() {
  it('empty on getMacroPluginDefinables', function() {
    expect(getMacroPluginDefinables([])).toHaveLength(0)
  })
  it('some on getMacroPluginDefinables', function() {
    const macroFunction = (env: Environment, slist: SList) => {
      return slist
    }
    const macroDefinables = [new MacroPluginDefinable('', macroFunction)]
    const definables = getMacroPluginDefinables([
      new Plugin('', macroDefinables)
    ])

    expect(definables).toHaveLength(1)
    expect(definables).toEqual(macroDefinables)
  })
})
