import {
  Environment as _Environment,
  _SList,
  _instanceOfs
} from '../../dist/index'
import { parseSExp } from '../../lib/parser/parse'
import { Service } from '../../lib/service'
import { reindentSProgram } from '../../lib/service/reindent'

describe('core', function() {
  describe('stringify with core', function() {
    let service: Service
    let reindentWithConfig: (code: string) => string
    beforeAll(function() {
      service = new Service([])

      reindentWithConfig = code =>
        reindentSProgram(service, service.s2program(parseSExp(code)))
    })
    it('for statement', function() {
      expect(
        reindentWithConfig(
          '[for let i 0; < i 100; inc i; (console.log i) (console.log (+ i 2))]'
        )
      ).toStrictEqual(
        `
(for
  let i 0;
  < i 100;
  inc i;
  (console.log i)
  (console.log (+ i 2))
)`.trim()
      )
    })
    it('do statement', function() {
      expect(
        reindentWithConfig(
          '[for let i 0; < i 100; inc i; {do (console.log i) (console.log (+ i 2))}]'
        )
      ).toStrictEqual(
        `
(for
  let i 0;
  < i 100;
  inc i;
  (do (console.log i) (console.log (+ i 2)))
)`.trim()
      )
    })
  })
})
