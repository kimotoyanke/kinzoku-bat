// @ts-nocheck
import { parseSExp } from '../lib/parser/parse'
import { identifierParser } from '../lib/parser/symbol'
import { instanceOfSList } from '../lib/s-ast/instance-of'
import { instanceOfSListSeparator } from '../lib/s-ast/list-type'
import { SIdentifier } from '../lib/s-ast/s-identifier'
import { SList } from '../lib/s-ast/s-list'
import { SString } from '../lib/s-ast/s-literal'
import { SMembers } from '../lib/s-ast/s-members'
import { SObjectPropertyIdentifier } from '../lib/s-ast/s-object-property'

describe('parser', function() {
  it('program is a array', function() {
    expect(parseSExp('[a b]')).toBeInstanceOf(Array)
    expect(parseSExp('[ a b]')).toBeInstanceOf(Array)
  })
  it('is list', function() {
    const parsedExcutableList = parseSExp('[a b]')[0] as SList
    const parsedUnexcutableList = parseSExp('[ a b]')[0] as SList
    expect(parsedExcutableList.isList).toBeFalsy()
    expect(parsedUnexcutableList).toBeTruthy()
  })
  it('executable list semi', function() {
    const parsedList = parseSExp(
      '[for let i 0; < i 10; inc i;{do (console.log i)}]'
    )[0] as SList
    expect(parsedList.expressions).toHaveLength(5)
    expect(
      parsedList
        .getLast()
        .slice(0, 3)
        .map(
          arg => instanceOfSList(arg) && instanceOfSListSeparator(arg.listType)
        )
    ).not.toContain(false)
  })
  it('unexcutable list semi', function() {
    const parsedList = parseSExp('[ 1 2 3; 4 5 6; 7 8 9; ]')[0] as SList
    expect(parsedList.expressions).toHaveLength(3)
  })

  it('parse hyphen capital', function() {
    const parsedList = parseSExp('(console-log "Hello, world")')[0] as SList
    const first = parsedList.expressions[0] as SIdentifier
    expect(first.name).toEqual('consoleLog')
  })
  it('parse first hyphen capital', function() {
    const parsedList = parseSExp('(-console-log "Hello, world")')[0] as SList
    const first = parsedList.expressions[0] as SIdentifier
    expect(first.name).toEqual('ConsoleLog')
  })

  it('2 identifiers member expression', function() {
    const parsedList = parseSExp('(console.log)')[0] as SList
    expect(parsedList.expressions[0]).toBeInstanceOf(SMembers)
    const members = parsedList.expressions[0] as SMembers
    expect((members.parent as SIdentifier).name).toEqual('console')
    expect(members.child.name).toEqual('log')
  })
  it('3 identifiers member expression', function() {
    const parsedList = parseSExp('(document.console.log)')[0] as SList
    const members = parsedList.expressions[0] as SMembers
    expect(members.child.name).toEqual('log')
    expect(((members.parent as SMembers).parent as SIdentifier).name).toEqual(
      'document'
    )
  })
  it("string literal's member expression", function() {
    const parsedList = parseSExp('("literal".toString)')[0] as SList
    const members = parsedList.expressions[0] as SMembers
    const parent = members.parent as SString
    expect(parent.value).toEqual('literal')
  })

  describe('literals', function() {
    it('string literal', function() {
      const str = 'Hello World'
      const stringLiteral = parseSExp(`("${str}")`)[0]['expressions'][0]
      expect(stringLiteral['value']).toEqual(str)
    })
    it('hex literal', function() {
      const hexLiteral = parseSExp(`(0x51)`)[0]['expressions'][0]
      expect(hexLiteral['value']).toEqual(0x51)
    })
    it('integer literal', function() {
      const numLiteral = parseSExp(`(51)`)[0]['expressions'][0]
      expect(numLiteral['value']).toEqual(51)
    })
    it('integer literal2', function() {
      const numLiteral = parseSExp(`(1)`)[0]['expressions'][0]
      expect(numLiteral['value']).toEqual(1)
    })
    it('float literal', function() {
      const numLiteral = parseSExp(`(51.1)`)[0]['expressions'][0]
      expect(numLiteral['value']).toEqual(51.1)
    })
    it('float literal2', function() {
      const numLiteral = parseSExp(`(.1)`)[0]['expressions'][0]
      expect(numLiteral['value']).toEqual(0.1)
    })
    it('expo literal', function() {
      const numLiteral = parseSExp(`(1.1e10)`)[0]['expressions'][0]
      expect(numLiteral['value']).toEqual(1.1e10)
    })
    it('minus numeral literal', function() {
      const numLiteral = parseSExp(`(-1.1e10)`)[0]['expressions'][0]
      expect(numLiteral['value']).toEqual(-1.1e10)
    })
  })
  describe('object', function() {
    it('object property name', function() {
      const objectPropertyNameAndValueLiteral: SList = parseSExp(
        `[key: me]`
      )[0] as SList
      expect(objectPropertyNameAndValueLiteral.isList).toBeFalsy()
      expect(objectPropertyNameAndValueLiteral.getFirst()).toBeInstanceOf(
        SObjectPropertyIdentifier
      )
    })
  })
  describe('xhani', function() {
    it('<a href="/">home</a>', function() {
      const str = '<a href="/">home</a>'
      const xLiteral = parseSExp(str)[0] as SList
      expect((xLiteral.getFirst() as SIdentifier).name).toEqual('h')
      expect(xLiteral.getLast()[0]).toBeInstanceOf(SString)
      expect(xLiteral.getLast()[0].toString()).toEqual('"a"')
    })

    const complexCode = `<Component @click-x.right="test"><a href="/">home</a></Component>`
    describe(complexCode, function() {
      let xLiteral: SList
      let child: SList
      beforeAll(function() {
        xLiteral = parseSExp(complexCode)[0] as SList
        child = xLiteral.getLast()[2] as SList
      })
      it('component parse', function() {
        expect((xLiteral.getFirst() as SIdentifier).name).toEqual('h')
        expect(xLiteral.getLast()[0]).toBeInstanceOf(SIdentifier)
        expect(xLiteral.getLast()[0].toString()).toEqual('Component')
      })
      it('component params', function() {
        const params = xLiteral.getLast()[1] as SList
        const param = params.expressions[0] as SList
        expect(param.expressions[0].toString()).toEqual('"@click-x.right":')
        expect(param.expressions[1].toString()).toEqual('"test"')
      })
      it('child parse', function() {
        expect((child.getFirst() as SIdentifier).name).toEqual('h')
        expect(child.getLast()[0]).toBeInstanceOf(SString)
        expect(child.getLast()[0].toString()).toEqual('"a"')
      })
      it('child params', function() {
        const childParams = child.getLast()[1] as SList
        const childParam = childParams.expressions[0] as SList
        expect(childParam.expressions[0].toString()).toEqual('"href":')
        expect(childParam.expressions[1].toString()).toEqual('"/"')
      })
    })
  })
  describe('reader-macro', function() {
    it('quote', function() {
      const str = "'(a b c)"
      const quoteLiteral = parseSExp(str)[0] as SList
      expect((quoteLiteral.getFirst() as SIdentifier).name).toEqual('quote')
    })
    it('quote string', function() {
      const str = '\'"aaa"'
      const quoteLiteral = parseSExp(str)[0] as SList
      expect((quoteLiteral.getFirst() as SIdentifier).name).toEqual('quote')
    })

    it('quasiquote list', function() {
      const str = '`{aaa}'
      const quoteLiteral = parseSExp(str)[0] as SList
      expect((quoteLiteral.getFirst() as SIdentifier).name).toEqual(
        'quasiquote'
      )
    })

    it('unquote', function() {
      const str = ',a'
      const unquoteLiteral = parseSExp(str)[0] as SList
      expect((unquoteLiteral.getFirst() as SIdentifier).name).toEqual('unquote')
    })
  })
  it('stoppable', function() {
    const str = `
[a [b [c [d [e [f [g [h [i [j [k [l [m [ 1 2 3 ] [n "aaa"]]]]]]]]]]]]]]

`

    expect(instanceOfSList(parseSExp(str)[0])).toBeTruthy()
  })
})
