import { mapTranspileDefault } from '../lib/transpiler/transpile'

import { parseSExp } from '../lib/parser/parse'

import generate from '@babel/generator'

describe('code in README.md', function() {
  it('1', function() {
    expect(
      mapTranspileDefault(
        parseSExp(`(console.log "Hello, World")
[console.log "Hello, World with square bracket!"]
{console.log "Hello, World with curly bracket!"}`)
      )
        .map(ast => generate(ast).code)
        .join('\n')
    ).toEqual(`console.log("Hello, World")
console.log("Hello, World with square bracket!")
console.log("Hello, World with curly bracket!")`)
  })
  it('2', function() {
    expect(
      mapTranspileDefault(
        parseSExp(
          `( console.log "If there's spaces between beginning bracket and first element, convert as a array")`
        )
      )
        .map(ast => generate(ast).code)
        .join('\n')
    ).toEqual(
      `[console.log, "If there's spaces between beginning bracket and first element, convert as a array"]`
    )
  })
  it('3', function() {
    expect(
      mapTranspileDefault(
        parseSExp(
          `[
1 2 3;
4 5 6;
7 8 9;
]
(console.log + 1 2; - 2 1;)`
        )
      )
        .map(ast => generate(ast).code)
        .join('\n')
    ).toEqual(
      `[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
console.log(1 + 2, 2 - 1)`
    )
  })
  it('4', function() {
    expect(
      mapTranspileDefault(parseSExp(`[console.log [window.to-string]]`))
        .map(ast => generate(ast).code)
        .join('')
    ).toEqual(`console.log(window.toString())`)
  })
})
