import generate from '@babel/generator'
import { Node } from '@babel/types'
import { watch } from 'chokidar'
import { readFile, unlink, writeFile } from 'fs-extra'
import { parseSExp } from 'kinzoku-bat/lib/parser/parse'
import { Service } from 'kinzoku-bat/lib/service'
import { Plugin } from 'kinzoku-bat/lib/service/plugin'
import { mapTranspileDefault } from 'kinzoku-bat/lib/transpiler/transpile'
import * as openEditor from 'open-editor'
import { addStatementOption } from 'plugin-core'
import { file } from 'tempy'

const inputFilepath = file({ extension: 'kbjs' })
const outputFilepath = file({ extension: 'js' })
const watcher = watch(inputFilepath)
const service = new Service(
  [new Plugin('plugin-core', [addStatementOption])],
  'kbjs',
  2
)
let cacheIn: string
let cacheOut: string
watcher.on('change', async () => {
  const code = await readFile(inputFilepath, 'utf8')
  const sexps = parseSExp(code)
  const s: string = service.stringify(sexps)
  if (s !== cacheIn) {
    await writeFile(inputFilepath, s)
    cacheIn = s
  }
  const jsAsts: Node[] = mapTranspileDefault(parseSExp(s))
  const jsCode: string = jsAsts.map(jsAst => generate(jsAst).code).join('\n')
  if (jsCode !== cacheOut) {
    await writeFile(outputFilepath, jsCode)
    cacheOut = jsCode
  }
  console.log(sexps)
})

openEditor([inputFilepath])
openEditor([outputFilepath])

console.log(inputFilepath, outputFilepath)

process.on('SIGINT', async () => {
  process.stdout.write('\nend\n')
  await unlink(inputFilepath)
  await unlink(outputFilepath)
  process.exit()
})
