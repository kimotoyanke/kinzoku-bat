import { watch } from 'chokidar'
import { readFile, writeFile } from 'fs-extra'
import { parseSExp } from 'kinzoku-bat/lib/parser/parse'
import { defaultService } from 'kinzoku-bat/lib/service'
import * as openEditor from 'open-editor'
import { file } from 'tempy'

const filepath = file({ extension: 'kbjs' })
const watcher = watch(filepath)
watcher.on('change', async () => {
  const code = await readFile(filepath, 'utf8')
  const sexps = parseSExp(code)
  const s = defaultService.stringify(sexps)
  await writeFile(filepath, s)
})

openEditor([filepath])
