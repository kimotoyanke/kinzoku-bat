import { Service, _instanceOfs as is, parseSExp } from 'kinzoku-bat'
import core from '.'
import { SList } from '../../dist/types/s-ast/s-list'
import { instanceOfSListSeparator } from '../../lib/s-ast/list-type'

describe('plugin-core', function() {
  describe('stringify with core', function() {
    let service: Service
    let reindentWithConfig: (code: string) => string
    beforeAll(function() {
      service = new Service([core])

      reindentWithConfig = code => service.stringify(parseSExp(code))
    })
    it('for statement', function() {
      const sexp = service.s2s(
        parseSExp(
          '[for let i 0; < i 100; inc i; (console.log i) (console.log (+ i 2))]'
        )[0]
      ) as SList

      expect(sexp.getFirst().toString()).toBe('for')
      expect(sexp.statementOption).toBe(3)
      expect(
        reindentWithConfig(
          '[for let i 0; < i 100; inc i; (console.log i) (console.log (+ i 2))]'
        )
      ).toStrictEqual(
        `
{for let i 0; < i 100; inc i;
  (console.log i)
  (console.log (+ i 2))
}`.trim()
      )
    })
    it('do statement', function() {
      expect(
        reindentWithConfig(
          '[for let i 0; < i 100; inc i; {do (console.log i) (console.log (+ i 2))}]'
        )
      ).toStrictEqual(
        `
{for let i 0; < i 100; inc i;
  (console.log i)
  (console.log (+ i 2))
}`.trim()
      )
    })
  })
  it('statement', function() {
    const sexp: SList = parseSExp(
      '[for let i 0; < i 100; [inc i] (console.log i) (console.log (+ i 2))]'
    )[0] as SList
    sexp.statementOption = 3
    expect(
      sexp
        .getLast()
        .slice(0, 3)
        .map(
          arg =>
            is.instanceOfSList(arg) && instanceOfSListSeparator(arg.listType)
        )
    ).toStrictEqual([true, true, false])

    const service = new Service([core])
    const s2sedSexp = service.s2s(sexp) as SList
    expect(
      s2sedSexp
        .getLast()
        .slice(0, 3)
        .map(
          arg =>
            is.instanceOfSList(arg) && instanceOfSListSeparator(arg.listType)
        )
    ).not.toContainEqual(false)

    expect(service.stringify([sexp])).toStrictEqual(
      `
{for let i 0; < i 100; inc i;
  (console.log i)
  (console.log (+ i 2))
}
      `.trim()
    )
  })
})
