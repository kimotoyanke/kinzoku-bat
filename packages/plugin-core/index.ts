// Coded as TypeScript temporary
import {
  MacroPluginDefinable,
  Plugin,
  _SIdentifier as SIdentifier,
  _SList as SList,
  _controlStatements as controlStatements,
  _instanceOfs as is,
  _listTypes as listTypes
} from 'kinzoku-bat'

const controlStatementsMap = controlStatements.reduce((p, c) => {
  return { ...p, [c.name]: c }
}, {})
export const executableListMustHaveBracket = new MacroPluginDefinable(
  'executableListMustHaveBracket',
  (env, slist) => {
    if (!slist.isList) {
      slist.listType = listTypes['round']
      return slist
    }
    return slist
  }
)

export const addStatementOption = new MacroPluginDefinable(
  'addStatementOption',
  (env, slist) => {
    const first = slist.getFirst()
    if (
      !slist.isList &&
      is.instanceOfSIdentifier(first) &&
      controlStatementsMap[first.name]
    ) {
      const {
        listType,
        statementOption,
        childrenListType
      } = controlStatementsMap[first.name].template
      const newSList = new SList(
        slist.expressions.map((e, i) => {
          /*console.log(
            i,

            childrenListType,
            childrenListType[i - 1],
            is.instanceOfSList(e)
          )*/
          if (childrenListType[i - 1] && is.instanceOfSList(e)) {
            return new SList(e.expressions, childrenListType[i - 1], e.isList)
          }
          return e
        }),
        listType
      )
      newSList.statementOption = statementOption
      return newSList
    }
    return slist
  }
)

export const lambdaAlias = new MacroPluginDefinable(
  'lambdaAlias',
  (env, slist) => {
    const ALIASES = ['lambda', 'fn', 'function', 'arrow']
    const first = slist.getFirst()
    if (
      !slist.isList &&
      is.instanceOfSIdentifier(first) &&
      ALIASES.includes(first.name)
    ) {
      const result = new SList(
        [new SIdentifier(String.fromCodePoint(0x03bb)), ...slist.getLast()],
        slist.listType,
        false
      )
      result.statementOption = slist.statementOption
      return result
    }
    return slist
  }
)

export default new Plugin('core', [
  lambdaAlias,
  executableListMustHaveBracket,
  addStatementOption
])
