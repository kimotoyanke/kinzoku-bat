import generate from '@babel/generator'
import { coreEnv, transpile } from 'kinzoku-bat'
import { createFilter } from '@rollup/pluginutils'

interface IOptions {
  include: string | string[]
  exclude?: string | string[]
}

const defaultOptions: IOptions = {
  include: '**/*.kbjs'
}

const kinzokuBat = (options: IOptions = defaultOptions) => {
  const filter = createFilter(options.include, options.exclude)
  const plugin = {
    name: 'kinzoku-bat',
    transform(
      code: string,
      id: string
    ): void | { code: string; map: { mappings: '' } } | null {
      if (!filter(id)) return null

      const env = coreEnv
      const resultCode = transpile(code, env)
      return {
        code: resultCode,
        map: { mappings: '' }
      }
    }
  }
  return plugin
}

export default kinzokuBat
