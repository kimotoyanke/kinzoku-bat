import generate from '@babel/generator'
import { parseSExp } from '../lib/parser/parse'
import { mapTranspileDefault } from '../lib/transpiler/transpile'
import { default as repl } from './repl'

const transpile = c =>
  mapTranspileDefault(parseSExp(c))
    .map(ast => generate(ast).code)
    .join('\n')
repl('transpile', transpile)
