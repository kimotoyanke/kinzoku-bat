import { parseSExp } from '../lib/parser/parse'
import { default as repl, printObject } from './repl'

repl('parse', parseSExp, printObject)
