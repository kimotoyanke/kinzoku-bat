import { transformSync } from '@babel/core'
import generate from '@babel/generator'
import { parseSExp } from '../lib/parser/parse'
import { mapTranspileDefault } from '../lib/transpiler/transpile'
import repl from './repl'

const evalKb = c => {
  const code = mapTranspileDefault(parseSExp(c))
    .map(ast => generate(ast).code)
    .join('\n')
  const transformed = transformSync(code, {
    presets: [
      [
        '@babel/preset-env',
        {
          useBuiltIns: 'entry'
        }
      ]
    ]
  }).code

  console.log(transformed)
  // tslint:disable-next-line:no-eval
  return eval(transformed)
}
repl('parse', evalKb)
