#!/usr/bin/env ts-node

import * as program from 'commander'
import { realpath } from 'fs-extra'
import { resolve } from 'path'

program
  .command('dev', 'development service', { isDefault: true })
  .option('-p, --project <directory>', 'project directory')
  .action(async options => {
    const project = options.project || './'
    console.log(project)
    const cwd = await realpath('./')
    const kbconfig = await require(resolve(cwd, 'kbconfig.js'))
    console.log('start watching')
    kbconfig.watch(resolve(project) + '/**/*')
  })

program.parse(process.argv)
