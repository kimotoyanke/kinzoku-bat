import { existsSync, mkdirSync } from 'fs'
import * as path from 'path'
import { render } from 'prettyjson'
// @ts-ignore: allowSyntheticDefaultImports
import promptSyncGenerator from 'prompt-sync'
// @ts-ignore: allowSyntheticDefaultImports
import promptSyncHistory from 'prompt-sync-history'

const homedir =
  process.env[process.platform === 'win32' ? 'USERPROFILE' : 'HOME']
export default (
  replName: string,
  replEval: (code: string) => any,
  print = console.log
) => {
  const historyFileDirectory = path.join(homedir, '/.cache/kinzoku-bat/')
  if (!existsSync(historyFileDirectory)) {
    mkdirSync(historyFileDirectory, { recursive: true })
  }
  const prompt = promptSyncGenerator({
    history: promptSyncHistory(path.join(historyFileDirectory, '.' + replName))
  })
  while (true) {
    const read: string = prompt(replName + '> ')
    if (read === null) {
      console.log(replName + ' will stop')
      // @ts-ignore
      prompt.history.save()
      return
    }
    if (read === '') {
      continue
    }
    // @ts-ignore
    prompt.history.save()
    try {
      print(replEval(read))
    } catch (e) {
      console.log(e)
    }
  }
}

export const printObject = output => console.log(render(output))
