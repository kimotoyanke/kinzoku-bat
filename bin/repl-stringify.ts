import { parseSExp } from '../lib/parser/parse'
import { Service } from '../lib/service'
import core from '../packages/plugin-core'
import { default as repl } from './repl'

const service = new Service([core])
const transpile = c => service.stringify(parseSExp(c))
repl('reindent', transpile)
