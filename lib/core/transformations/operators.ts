import * as t from '@babel/types'
import { Transformation } from '.'
import { SExp } from '../../s-ast'
import { Operator, operators } from '../../s-ast/operator'
import { Environment } from '../../transpiler/env'
import { transpileAsExpression } from '../../transpiler/transpile'
import { singleOperators } from './single-operators'

const getBinaryOperatorTransformation = (op: Operator) => {
  const func = (env: Environment, ...nums: SExp[]): t.Expression => {
    if (nums.length < 2) {
      throw new Error(
        `"${op}" expects more or equal than 2 elements; we get ${
          nums.length
        } element${nums.length === 1 ? '' : 's'}.`
      )
    }
    if (nums.length === 2) {
      return t.binaryExpression(
        op,
        transpileAsExpression(env, nums[0]),
        transpileAsExpression(env, nums[1])
      )
    }
    return t.binaryExpression(
      op,
      transpileAsExpression(env, nums[0]),
      func(env, ...nums.slice(1))
    )
  }
  return func
}
const operatorTransformations: {
  [key: string]: Transformation
} = {
  ...operators.reduce((p, c) => {
    return { ...p, [c]: getBinaryOperatorTransformation(c) }
  }, {})
}
export default { ...operatorTransformations, ...singleOperators }
