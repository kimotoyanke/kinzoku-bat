import * as t from '@babel/types'
import { TransformationDictionary } from '.'
import { SExp } from '../../s-ast'
import { instanceOfSIdentifier, instanceOfSList } from '../../s-ast/instance-of'
import { SIdentifier } from '../../s-ast/s-identifier'
import { SList, SUnexcutableList } from '../../s-ast/s-list'
import { Environment } from '../../transpiler/env'
import {
  transpileAsExpression,
  transpileSExp
} from '../../transpiler/transpile'

type variableDeclarationToken = 'let' | 'const' | 'var'
const variableDeclarationTokens: variableDeclarationToken[] = [
  'let',
  'const',
  'var'
]

const variableDeclarations: TransformationDictionary = {
  ...variableDeclarationTokens
    .map((token: variableDeclarationToken) => ({
      [token]: (env: Environment, ...args: SExp[]) => {
        const firstArg = args[0]
        if (instanceOfSIdentifier(firstArg)) {
          const id: SIdentifier | SUnexcutableList = firstArg
          const init: SExp = args[1]
          const idLVal: t.LVal = transpileSExp(env, id) as t.LVal
          if (!t.isLVal(idLVal)) {
            throw new Error('id of declarators should be a LVal')
          }
          return t.variableDeclaration(token, [
            t.variableDeclarator(
              idLVal,
              init ? transpileAsExpression(env, init) : null
            )
          ])
        } else {
          const declarators = args.map(arg => {
            if (instanceOfSList(arg)) {
              const id: SExp = arg.expressions[0]
              if (!instanceOfSIdentifier(id)) {
                throw new Error('id of declarators should be a identifier')
              } else {
                const init: SExp = arg.expressions[1]
                return t.variableDeclarator(
                  t.identifier(id.name),
                  init ? transpileAsExpression(env, init) : null
                )
              }
            } else {
              throw new Error('declarators should be a List')
            }
          })
          return t.variableDeclaration(token, declarators)
        }
      }
    }))
    .reduce((p, c) => ({ ...p, ...c }), {})
}

export default variableDeclarations
