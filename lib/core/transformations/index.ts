import * as t from '@babel/types'
import { fromPairs, map, toPairs } from 'ramda'
import { createSIdentifierName } from '../../parser/symbol'
import { SExp } from '../../s-ast'
import { Environment } from '../../transpiler/env'
import newExpressions from './new-expression'
import operatorExpressions from './operators'
import statementsDictionary from './statements'
import variableDeclarations from './variable-declations'

export type Transformation = (env: Environment, ...args: SExp[]) => t.Node
export type TransformationDictionary = { [key: string]: Transformation }

const keyMap = <T>(
  f: (str: string) => string,
  obj: { [name: string]: T }
): { [name: string]: T } => {
  const pairs = toPairs(obj)
  const fPairs = map(([k, v]) => [f(k), v], pairs) as [string, T][]
  return fromPairs(fPairs)
}
export default {
  ...keyMap(createSIdentifierName, {
    ...statementsDictionary,
    ...variableDeclarations,
    ...newExpressions
  }),
  ...operatorExpressions
}
