import * as t from '@babel/types'
import { TransformationDictionary } from '.'
import { SExp } from '../../s-ast'
import { Environment } from '../../transpiler/env'
import { transpileAsExpression } from '../../transpiler/transpile'

const newExpressions: TransformationDictionary = {
  ['new'](env: Environment, callee, ...args: SExp[]) {
    return t.newExpression(
      transpileAsExpression(env, callee),
      args.map(transpileAsExpression(env))
    )
  }
}
export default newExpressions
