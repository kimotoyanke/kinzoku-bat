import * as t from '@babel/types'
import { TransformationDictionary } from '.'
import { SExp } from '../../s-ast'
import { transpileAsExpression } from '../../transpiler/transpile'

export const singleOperators: TransformationDictionary = {
  ['inc'](env, i: SExp) {
    return t.updateExpression('++', transpileAsExpression(env, i))
  },
  ['dec'](env, i: SExp) {
    return t.updateExpression('--', transpileAsExpression(env, i))
  }
}
