import * as t from '@babel/types'
import { init, last } from 'ramda'
import { TransformationDictionary } from '.'
import { elementParser } from '../../parser/parse'
import { SExp } from '../../s-ast'
import { instanceOfSIdentifier, instanceOfSList } from '../../s-ast/instance-of'
import { SList } from '../../s-ast/s-list'
import { Environment } from '../../transpiler/env'
import {
  transpileAsDeclaration,
  transpileAsExpression,
  transpileAsIdentifier,
  transpileAsLVal,
  transpileAsStatement,
  transpileAsStringLiteral,
  transpileAsVariableDeclaration,
  transpileSExp
} from '../../transpiler/transpile'

const moduleStatements: TransformationDictionary = {
  ['import-from'](env: Environment, variable: SExp, mod: SExp, ..._: SExp[]) {
    const modName: t.StringLiteral = transpileAsStringLiteral(env, mod)
    if (instanceOfSIdentifier(variable)) {
      return t.importDeclaration(
        [t.importDefaultSpecifier(t.identifier(variable.name))],
        modName
      )
    } else if (instanceOfSList(variable)) {
      return t.importDeclaration(
        variable.expressions.map(i =>
          t.importSpecifier(
            transpileAsIdentifier(env, i),
            transpileAsIdentifier(env, i)
          )
        ),
        modName
      )
    }
    return t.importDeclaration([], t.stringLiteral(''))
  },
  ['export'](env: Environment, ...params: SExp[]) {
    if (params[0]) {
      const exported = params[0]
      if (instanceOfSList(exported)) {
        return t.exportNamedDeclaration(
          transpileAsDeclaration(env, exported),
          []
        )
      } else if (instanceOfSIdentifier(exported)) {
        return t.exportDefaultDeclaration(transpileAsIdentifier(env, exported))
      }
    }
    return t.exportDefaultDeclaration(t.objectExpression([]))
  },
  ['export-default'](
    env: Environment,
    ...params: SExp[]
  ): t.ExportDefaultDeclaration {
    if (params[0]) {
      const exported = params[0]
      if (instanceOfSList(exported)) {
        return t.exportDefaultDeclaration(transpileAsExpression(env, exported))
      } else if (instanceOfSIdentifier(exported)) {
        return t.exportDefaultDeclaration(transpileAsIdentifier(env, exported))
      }
    }
    return t.exportDefaultDeclaration(t.objectExpression([]))
  }
}

const spreadElements: TransformationDictionary = {
  ['spread'](env: Environment, spreadable) {
    return t.spreadElement(transpileAsExpression(env, spreadable))
  }
}

const operationStatements: TransformationDictionary = {
  ['get-property'](env: Environment, nth, list) {
    return t.memberExpression(
      transpileAsExpression(env, list),
      transpileAsExpression(env, nth),
      true
    )
  }
}

const assignmentStatements: TransformationDictionary = {
  ['set'](env: Environment, a, b) {
    return t.assignmentExpression(
      '=',
      transpileAsLVal(env, a),
      transpileAsExpression(env, b)
    )
  }
}

const controlStatements: TransformationDictionary = {
  ['for'](env: Environment, init, test, update, ...body) {
    return t.forStatement(
      transpileAsVariableDeclaration(env, init),
      transpileAsExpression(env, test),
      transpileAsExpression(env, update),
      t.blockStatement(body.map(transpileAsStatement(env)))
    )
  },
  ['do'](env: Environment, ...statements) {
    return t.blockStatement(statements.map(transpileAsStatement(env)))
  },
  ['if'](env: Environment, condition, ...body) {
    const lastArg = last(body)

    if (lastArg && instanceOfSList(lastArg)) {
      const elseIdentifier = lastArg.getFirst()
      if (
        instanceOfSIdentifier(elseIdentifier) &&
        elseIdentifier.name === 'else'
      ) {
        return t.ifStatement(
          transpileAsExpression(env, condition),
          t.blockStatement(init(body).map(transpileAsStatement(env))),
          t.blockStatement(lastArg.getLast().map(transpileAsStatement(env)))
        )
      }
    }
    return t.ifStatement(
      transpileAsExpression(env, condition),
      t.blockStatement(body.map(transpileAsStatement(env)))
    )
  },
  ['\u03BB'](env: Environment, params: SExp, ...body) {
    if (instanceOfSList(params)) {
      return t.arrowFunctionExpression(
        params.expressions.map(transpileAsIdentifier(env)),
        t.blockStatement(body.map(transpileAsStatement(env)))
      )
    } else {
      return t.arrowFunctionExpression(
        [],
        t.blockStatement(body.map(transpileAsStatement(env)))
      )
    }
  },
  ['else'](env, ...body) {
    throw new Error(`Do not call "else" outside if statement`)
  }
}

const statementsDictionary: TransformationDictionary = {
  ...controlStatements,
  ...moduleStatements,
  ...spreadElements,
  ...operationStatements,
  ...assignmentStatements
}

export default statementsDictionary
