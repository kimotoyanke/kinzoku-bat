import { repeat } from 'ramda'
import {
  SListType,
  bracketListTypes,
  separatorListTypes
} from '../s-ast/list-type'
import { SStatementOption } from '../s-ast/s-list'

export type ControlStatement = {
  name: string
  template: {
    listType: SListType
    childrenListType: (SListType | undefined)[]
    statementOption: SStatementOption
  }
}

export const controlStatements: ControlStatement[] = [
  {
    name: 'for',
    template: {
      listType: bracketListTypes.curly,
      childrenListType: repeat(separatorListTypes.semi, 3),
      statementOption: 3
    }
  },
  {
    name: 'do',
    template: {
      listType: bracketListTypes.curly,
      childrenListType: [],
      statementOption: 0
    }
  },
  {
    name: 'if',
    template: {
      listType: bracketListTypes.curly,
      childrenListType: [bracketListTypes.round],
      statementOption: 1
    }
  },
  {
    name: 'else',
    template: {
      listType: bracketListTypes.curly,
      childrenListType: [],
      statementOption: 0
    }
  },
  {
    name: '\u03BB',
    template: {
      listType: bracketListTypes.curly,
      childrenListType: [bracketListTypes.square],
      statementOption: 1
    }
  }
]
