import { Macro } from '.'
import { createSIdentifier } from '../../parser/symbol'
import { SList } from '../../s-ast/s-list'
import { Environment } from '../../transpiler/env'

const createAlias = (newName: string): Macro => {
  return (_: Environment, args: SList) => {
    const newSList = new SList(
      [createSIdentifier(newName), ...args.getLast()],
      args.listType,
      args.isList
    )
    return newSList
  }
}
const lambdaAlias = createAlias('λ')

const defaultAliases: { [name: string]: Macro } = {
  ['lambda']: lambdaAlias,
  ['function']: lambdaAlias,
  ['fn']: lambdaAlias
}

export default defaultAliases
