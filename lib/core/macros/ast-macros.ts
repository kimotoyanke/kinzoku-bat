import { curry, reduce } from 'ramda'
import { Macro } from '.'
import { createSIdentifier } from '../../parser/symbol'
import { SExp } from '../../s-ast'
import {
  instanceOfSIdentifier,
  instanceOfSList,
  instanceOfSMembers,
  instanceOfSObjectPropertyIdentifier,
  instanceOfSObjectPropertyNumeric,
  instanceOfSObjectPropertyString,
  instanceOfSString
} from '../../s-ast/instance-of'
import { SListType, bracketListTypes } from '../../s-ast/list-type'
import { SIdentifier } from '../../s-ast/s-identifier'
import { SList } from '../../s-ast/s-list'
import { SBoolean, SNumeric, SString } from '../../s-ast/s-literal'
import { SMembers } from '../../s-ast/s-members'
import { Environment } from '../../transpiler/env'

const createNewSList = (className: string, args: SExp[]): SList => {
  return new SList([
    createSIdentifier('new'),
    createSIdentifier(className),
    ...args
  ])
}

const createNewSListForSList = (
  env: Environment,
  expressions: SExp[],
  type: SListType,
  isList: boolean
): SList => {
  env.kbImports.add('_SList')
  env.kbImports.add('_listTypes')
  return createNewSList('_SList', [
    new SList(expressions, bracketListTypes.round, true),
    createStringNthList(type.name, createSIdentifier('_list-types')),
    new SBoolean(isList)
  ])
}
const createNewSListForSMembers = (
  env: Environment,
  parent: SExp,
  child: SExp
): SList => {
  env.kbImports.add('_SMembers')
  return createNewSList('_SMembers', [parent, child])
}

const createNewSListForSMembersUnquoted = (
  env: Environment,
  parent: SExp,
  child: SIdentifier
): SList => {
  env.kbImports.add('_SMembers')
  if (instanceOfSMembers(parent)) {
    return createNewSList('_SMembers', [
      createNewSListForSMembersUnquoted(env, parent.parent, parent.child),
      quasiquoteSExp(env, child)
    ])
  }
  return createNewSList('_SMembers', [parent, quasiquoteSExp(env, child)])
}

const createStringNthList = (propertyName: string, parent: SExp): SList => {
  return new SList([
    createSIdentifier('get-property'),
    new SString(propertyName),
    parent
  ])
}

const quoteSExp = curry(
  (env: Environment, arg: SExp): SList => {
    const sIdentifierArg = instanceOfSIdentifier(arg)
    if (instanceOfSList(arg)) {
      return createNewSListForSList(
        env,
        arg.expressions.map(quoteSExp(env)),
        arg.listType,
        arg.isList
      )
    } else if (instanceOfSIdentifier(arg)) {
      env.kbImports.add('_SIdentifier')
      return createNewSList('_SIdentifier', [new SString(arg.name)])
    } else if (instanceOfSMembers(arg)) {
      return createNewSListForSMembers(
        env,
        quoteSExp(env, arg.parent),
        quoteSExp(env, arg.child)
      )
    }
    return createNewSList('error', [])
  }
)

const quote = curry((env: Environment, l: SList) => {
  const arg = l.getLast()[0]
  if (instanceOfSList(arg)) {
    env.kbImports.add('list-types')
    return quoteSExp(env, arg)
  }
  return quoteSExp(env, arg)
})

const quasiquoteSList = (env: Environment, arg: SList): SExp => {
  const first = arg.getFirst()
  if (instanceOfSIdentifier(first) && first.name === 'unquote') {
    return arg.getLast()[0]
  }
  return createNewSListForSList(
    env,
    arg.expressions.map(quasiquoteSExp(env)),
    arg.listType,
    arg.isList
  )
}

const quasiquoteSExp = curry(
  (env: Environment, arg: SExp): SExp => {
    if (instanceOfSList(arg)) {
      return quasiquoteSList(env, arg)
    } else if (instanceOfSIdentifier(arg)) {
      env.kbImports.add('_SIdentifier')
      return createNewSList('_SIdentifier', [new SString(arg.name)])
    } else if (instanceOfSMembers(arg)) {
      return createNewSListForSMembers(
        env,
        quasiquoteSExp(env, arg.parent),
        quasiquoteSExp(env, arg.child)
      )
    } else if (instanceOfSString(arg)) {
      env.kbImports.add('_SString')
      return createNewSList('_SString', [new SString(arg.value)])
    } else if (instanceOfSObjectPropertyString(arg)) {
      env.kbImports.add('_SObjectPropertyString')
      return createNewSList('_SObjectPropertyString', [new SString(arg.name)])
    } else if (instanceOfSObjectPropertyNumeric(arg)) {
      env.kbImports.add('_SObjectPropertyNumeric')
      return createNewSList('_SObjectPropertyNumeric', [new SNumeric(arg.name)])
    } else if (instanceOfSObjectPropertyIdentifier(arg)) {
      env.kbImports.add('_SObjectPropertyIdentifier')
      return createNewSList('_SObjectPropertyIdentifier', [
        new SString(arg.name)
      ])
    }
    return arg
  }
)

const quasiquote = curry(
  (env: Environment, l: SList): SExp => {
    const args = l.getLast()
    if (args.length === 1) {
      return quasiquoteSExp(env, args[0])
    }
    const doExpression = new SList(
      [new SIdentifier('do'), ...args],
      bracketListTypes['round']
    )
    return quasiquoteSExp(env, doExpression)
  }
)

const astMacros: { [name: string]: Macro } = {
  ['list']: (env, l) => {
    env.kbImports.add('_SList')
    return createNewSList('_SList', l.getLast())
  },
  ['_Symbol']: (env, l) => {
    env.kbImports.add('_SIdentifier')
    return createNewSList('_SIdentifier', [l.getLast()[0]])
  },
  ['quote']: quote,
  ['quasiquote']: quasiquote
}

export default astMacros
