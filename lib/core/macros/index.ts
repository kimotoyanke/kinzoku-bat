import { SExp } from '../../s-ast'
import { SList } from '../../s-ast/s-list'
import { Environment } from '../../transpiler/env'
import defaultAliases from './aliases'
import astMacros from './ast-macros'

export type Macro = (env: Environment, args: SList) => SExp
export default {
  ...astMacros,
  ...defaultAliases
}
