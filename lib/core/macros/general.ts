import { Macro } from '.'
import { Environment } from '../..'
import { instanceOfSIdentifier, instanceOfSList } from '../../s-ast/instance-of'
import { SList } from '../../s-ast/s-list'
import { ControlStatement, controlStatements } from '../control-statement'

const controlStatementsMap = controlStatements.reduce(
  (p, c) => {
    return { ...p, [c.name]: c }
  },
  {} as {
    [name: string]: ControlStatement
  }
)

export const generalMacrosForSList: Macro[] = [
  (env: Environment, slist: SList) => {
    const first = slist.getFirst()
    if (
      !slist.isList &&
      instanceOfSIdentifier(first) &&
      controlStatementsMap[first.name]
    ) {
      const {
        listType,
        statementOption,
        childrenListType
      } = controlStatementsMap[first.name].template
      const newSList = new SList(
        slist.expressions.map((e, i) => {
          if (childrenListType[i] && instanceOfSList(e)) {
            return new SList(e.expressions, childrenListType[i], e.isList)
          }
          return e
        }),
        listType
      )
      newSList.statementOption = statementOption
      return newSList
    }
    throw new Error('Macro Error ' + slist.toString())
  }
]
