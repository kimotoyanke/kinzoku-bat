import { extname } from 'path'
import { toPairs } from 'ramda'
import { Service } from '.'
import { transpileFileAsCache } from '../file/transpile'
import { CoreEnvironment } from '../transpiler/env'
import {
  BoilerplatePluginDefinable,
  MacroPluginDefinable,
  Plugin,
  PluginDefinable
} from './plugin'

export type Config = {}
export type ServiceConfigFile = {
  plugins: string[]
  ext: string
  indent: number | 'tab'
}

type PluginFile = { [name: string]: PluginDefinable }

export const getService = async (
  config: ServiceConfigFile
): Promise<Service> => {
  const promises: Promise<Plugin>[] = config.plugins.map(
    async (pluginPath: string): Promise<Plugin> => {
      const jsPluginPath: string = await (async (): Promise<string> => {
        if (extname(pluginPath) === config.ext) {
          return await transpileFileAsCache(new CoreEnvironment(), pluginPath)
        } else {
          return pluginPath
        }
      })()
      const pluginFile = (await require(jsPluginPath)) as PluginFile
      return new Plugin(
        jsPluginPath,
        toPairs(pluginFile)
          .map(([name, definable]) => definable)
          .filter(definable => definable instanceof PluginDefinable)
      )
    }
  )

  const plugins: Plugin[] = await Promise.all(promises)
  return new Service(plugins)
}

export const getMacroPluginDefinables = (
  plugins: Plugin[]
): MacroPluginDefinable[] => {
  return plugins
    .reduce(
      (p: PluginDefinable[], c: Plugin): PluginDefinable[] => [
        ...p,
        ...c.definables
      ],
      []
    )
    .filter(d => isMacroPluginDefinable(d)) as MacroPluginDefinable[]
}

export const getBoilerplatePluginDefinables = (
  plugins: Plugin[]
): BoilerplatePluginDefinable[] => {
  return plugins
    .reduce(
      (p: PluginDefinable[], c: Plugin): PluginDefinable[] => [
        ...p,
        ...c.definables
      ],
      []
    )
    .filter(d => isBoilerplateDefinable(d)) as BoilerplatePluginDefinable[]
}

export const isMacroPluginDefinable = (d: any): d is MacroPluginDefinable => {
  return d && d.apply && d.type === 'macro' && typeof d.apply === 'function'
}

const isBoilerplateDefinable = (d: any): d is BoilerplatePluginDefinable => {
  return (
    d &&
    d.apply &&
    d.type === 'boilerplate' &&
    typeof d.apply === 'function' &&
    d.fileglobs.length >= 0
  )
}
