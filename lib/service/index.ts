import chokidar from 'chokidar'
import { readFile, writeFile } from 'fs-extra'
import minimatch from 'minimatch'
import { string } from 'parsimmon'
import { relative } from 'path'
import { parseSExp } from '../parser/parse'
import { SExp, SProgram } from '../s-ast'
import {
  instanceOfSExp,
  instanceOfSIdentifier,
  instanceOfSList
} from '../s-ast/instance-of'
import { SList } from '../s-ast/s-list'
import { Environment, coreEnv } from '../transpiler/env'
import { mapTranspileDefault } from '../transpiler/transpile'
import {
  getBoilerplatePluginDefinables,
  getMacroPluginDefinables,
  isMacroPluginDefinable
} from './config'
import {
  BoilerplatePluginDefinable,
  MacroPluginDefinable,
  Plugin
} from './plugin'
import { reindentSExp, reindentSProgram } from './reindent'

const s2programStatementBody = (service: Service, slist: SList) => {
  const args = slist.getLast()
  const options = args.slice(0, slist.statementOption)
  const body = args.slice(slist.statementOption)
  const result = new SList(
    [slist.getFirst(), ...options, ...service.s2program(body)],
    slist.listType,
    slist.isList
  )
  result.statementOption = slist.statementOption
  return result
}

export class Service {
  plugins: Plugin[]
  ext: string = '.kbjs'
  indent: number | 'tab' = 2
  files: string = '**/*' + this.ext
  encoding: string = 'utf-8'
  exclude: (string | RegExp)[] = [/node_modules/]
  get macros(): MacroPluginDefinable[] {
    return getMacroPluginDefinables(this.plugins)
  }
  get boilerplates(): BoilerplatePluginDefinable[] {
    return getBoilerplatePluginDefinables(this.plugins)
  }

  get excludeRegExps(): RegExp[] {
    return this.exclude.map(regexp => {
      if (typeof regexp === 'string') {
        return new RegExp(regexp)
      }
      return regexp
    })
  }

  // S-expression to S-expression
  s2s(sexp: SExp): SExp {
    let env: Environment = coreEnv
    const s2sToExpressions = (s: SList) => {
      return new SList(
        [s.getFirst(), ...s.getLast().map(s => this.s2s(s))],
        s.listType,
        s.isList
      )
    }
    if (instanceOfSList(sexp)) {
      return this.macros.reduce(
        (p: SExp, d: MacroPluginDefinable) =>
          instanceOfSList(p)
            ? (() => {
                let applied = d.apply(env, s2sToExpressions(p))
                if (
                  instanceOfSList(applied) &&
                  applied.statementOption !== undefined
                ) {
                  const result = s2programStatementBody(this, applied)
                  return result
                }

                return applied
              })()
            : p,
        sexp
      )
    } else {
      return sexp
    }
  }

  // S-expression to Program
  s2program(sprogram: SProgram): SProgram {
    const isDoExpression = (slist: SList): boolean => {
      const first = slist.getFirst()
      return (
        !slist.isList && instanceOfSIdentifier(first) && first.name === 'do'
      )
    }

    if (sprogram.length === 0) {
      return []
    }
    const s2sed = this.s2s(sprogram[0])
    if (instanceOfSList(s2sed) && isDoExpression(s2sed)) {
      return [...s2sed.getLast(), ...this.s2program(sprogram.slice(1))]
    }
    return [s2sed, ...this.s2program(sprogram.slice(1))]
  }

  constructor(plugins: Plugin[]) {
    this.plugins = plugins
  }

  reindentSExp(sexp: SExp, indented: number = 0, firstIndent = true): string {
    return reindentSExp(this, sexp, indented, firstIndent)
  }
  reindentSProgram(
    sprogram: SProgram,
    indented: number = 0,
    firstIndent = true
  ): string {
    return reindentSProgram(this, sprogram, indented, firstIndent)
  }

  stringify(sexps: SExp[]): string {
    const ast = this.s2program(sexps)
    return this.reindentSProgram(ast)
  }

  async pushBoilerplate(
    newFilepath: string,
    boilerplate: BoilerplatePluginDefinable
  ) {
    const data = await readFile(newFilepath, {
      encoding: this.encoding
    })
    if (data.length > 0) {
      return
    }
    const sexp = boilerplate.apply(newFilepath)
    if (!instanceOfSExp(sexp)) {
      throw new Error(
        `${
          boilerplate.name
        }: boilerplate doesn't return a SExp: ${JSON.stringify(sexp)}`
      )
    }
    const program: SProgram = (() => {
      if (instanceOfSExp(sexp) && instanceOfSList(sexp) && !sexp.isList) {
        const first = sexp.getFirst()
        if (instanceOfSIdentifier(first)) {
          if (first.name === 'do') {
            return sexp.getLast()
          }
        }
      }
      if (instanceOfSExp(sexp)) {
        return [sexp]
      }
      return sexp
    })()
    const code = this.stringify(program)
    console.log(sexp, program, code)
    await writeFile(newFilepath, code, { encoding: this.encoding })
  }

  isKbPath(path: string): boolean {
    return path.endsWith(this.ext)
  }

  _DEBUG: boolean = false
  log(...args: any[]) {
    if (this._DEBUG) {
      console.log(...args)
    }
  }

  watch(files?: string) {
    const watcher = chokidar.watch(files || this.files, {
      ignored: this.excludeRegExps
    })
    const fileCache: Map<string, string> = new Map()
    watcher
      // Boilerplate
      .on('add', async (fullpath: string) => {
        console.log('BP: ' + fullpath)
        const rpath = relative(process.cwd(), fullpath)
        console.log('BP relative: ' + rpath)
        if (!this.isKbPath(fullpath)) {
          console.log('BP not kbpath: ' + fullpath)
          return
        }
        console.log('BP num: ' + this.boilerplates.length)
        const boilerplate = this.boilerplates.filter(bp =>
          bp.fileglobs.some(glob => minimatch(rpath, glob))
        )[0]
        if (boilerplate) {
          console.log('BP push: ' + fullpath)
          await this.pushBoilerplate(fullpath, boilerplate)
          console.log('BP pushed: ' + fullpath)
        }
      })
      // S2S
      .on('change', async (path: string) => {
        console.log('S2S: ' + path)
        if (!this.isKbPath(path)) {
          console.log('S2S not kbpath: ' + path)
          return
        }
        const code = await readFile(path, {
          encoding: this.encoding
        })
        console.log('S2S readed: ' + path)
        if (fileCache.get(path) === code) {
          console.log('S2S ignored: ' + path)
          return
        }
        console.log('S2S parse')
        const sprogram = parseSExp(code)
        console.log('S2S parsed: ' + path)
        const newCode = this.stringify(sprogram)
        console.log('S2S stringified: ' + path)
        await watcher.unwatch(path)
        await writeFile(path, newCode, { encoding: this.encoding })
        console.log('S2S writed: ' + path)
        fileCache.set(path, newCode)
        await watcher.add(path)
      })
  }
}

export const defaultService = new Service([])
