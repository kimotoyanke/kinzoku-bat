import { curry } from 'ramda'
import { Service } from '.'
import {
  SListTypeReaderMacro,
  isSListTypeReaderMacro,
  readerMacros
} from '../parser/reader-macro'
import { SExp, SProgram } from '../s-ast'
import {
  instanceOfSIdentifier,
  instanceOfSList,
  instanceOfSMembers
} from '../s-ast/instance-of'
import { instanceOfSListSeparator, isSBracket } from '../s-ast/list-type'
import { SList } from '../s-ast/s-list'

const getIndent = curry((indentType: number | 'tab', indented: number) => {
  if (indentType === 'tab') {
    return '\t'.repeat(indented)
  } else {
    return ' '.repeat(indentType).repeat(indented)
  }
})

const reindentAsSingleColumn = (
  service: Service,
  slist: SList,
  indented: number,
  firstIndent: boolean
): string => {
  const indent = getIndent(service.indent, indented)
  const fIndent = firstIndent ? indent : ''

  if (instanceOfSListSeparator(slist.listType)) {
    return (
      fIndent +
      slist.expressions
        .map(s => reindentSExp(service, s, indented, false))
        .join(' ') +
      slist.listType.separator
    )
  }
  const readerMacro = getReaderMacro(slist)
  if (readerMacro) {
    return (
      fIndent +
      readerMacro.char +
      reindentSExp(
        service,
        readerMacro.reduce(slist.getLast()),
        indented,
        false
      )
    )
  }

  if (isSBracket(slist.listType)) {
    if (slist.isList) {
      return (
        fIndent +
        [
          slist.defaultBracket.start,
          ...slist.expressions.map(s =>
            reindentSExp(service, s, indented, false)
          ),
          slist.defaultBracket.end
        ].join(' ')
      )
    } else {
      return (
        fIndent +
        [
          slist.defaultBracket.start +
            reindentSExp(service, slist.getFirst(), indented, false),
          ' ',
          slist
            .getLast()
            .map(s => reindentSExp(service, s, indented, false))
            .join(' '),
          slist.defaultBracket.end
        ].join('')
      )
    }
  }

  return (
    fIndent +
    [
      slist.defaultBracket.start +
        reindentSExp(service, slist.getFirst(), indented, false),
      ' ',
      slist
        .getLast()
        .map(s => reindentSExp(service, s, indented, false))
        .join(' '),
      slist.defaultBracket.end
    ].join('')
  )
}

const getReaderMacro = (slist: SList): SListTypeReaderMacro | null => {
  if (isSListTypeReaderMacro(slist.listType)) {
    return slist.listType
  }
  const first = slist.getFirst()
  if (instanceOfSIdentifier(first)) {
    const matchesReaderMacros = readerMacros.filter(
      ({ char, funcName }) => funcName === first.name
    )
    if (matchesReaderMacros.length > 0) {
      return matchesReaderMacros[0]
    }
  }
  return null
}

const reindentAsMultipleColumn = (
  service: Service,
  slist: SList,
  indented: number,
  firstIndent: boolean
): string => {
  const indent = getIndent(service.indent, indented)
  const fIndent = firstIndent ? indent : ''
  if (instanceOfSListSeparator(slist.listType)) {
    return (
      fIndent +
      slist.expressions
        .map(s => reindentSExp(service, s, indented, false))
        .join('\n') +
      slist.listType.separator
    )
  }
  const readerMacro = getReaderMacro(slist)
  if (readerMacro) {
    return (
      fIndent +
      readerMacro.char +
      reindentSExp(
        service,
        readerMacro.reduce(slist.getLast()),
        indented,
        false
      )
    )
  }

  if (isSBracket(slist.listType)) {
    if (slist.isList) {
      return [
        fIndent + slist.defaultBracket.start,
        ...slist.expressions.map(s => reindentSExp(service, s, indented + 1)),
        indent + slist.defaultBracket.end
      ].join('\n')
    } else {
      return [
        fIndent +
          slist.defaultBracket.start +
          reindentSExp(service, slist.getFirst(), indented + 1, false),
        ...slist.getLast().map(s => reindentSExp(service, s, indented + 1)),
        indent + slist.defaultBracket.end
      ].join('\n')
    }
  }
  return reindentAsSingleColumn(service, slist, indented, firstIndent)
}

const reindentSList = (
  service: Service,
  slist: SList,
  indented: number = 0,
  firstIndent = true
): string => {
  if (
    slist.statementOption !== undefined &&
    !slist.isList &&
    isSBracket(slist.listType)
  ) {
    const indent = getIndent(service.indent, indented)
    const fIndent = firstIndent ? indent : ''
    const args = slist.getLast()
    const options = args.slice(0, slist.statementOption)
    const body = args.slice(slist.statementOption)
    return [
      [
        fIndent +
          slist.listType.start +
          reindentSExp(service, slist.getFirst(), indented + 1, false),
        ...options.map(s => reindentSExp(service, s, indented + 1, false))
      ].join(' '),
      ...body.map(s => service.reindentSExp(s, indented + 1)),
      indent + slist.listType.end
    ].join('\n')
  }

  const single = reindentAsSingleColumn(service, slist, indented, firstIndent)
  if (single.split('\n').every(s => s.length < 60)) {
    return single
  } else {
    return reindentAsMultipleColumn(service, slist, indented, firstIndent)
  }
}

export const reindentSExp = (
  service: Service,
  sexp: SExp | null,
  indented: number = 0,
  firstIndent: boolean = true
): string => {
  if (sexp === null) {
    return ''
  }
  const indent = getIndent(service.indent, indented)
  const fIndent = firstIndent ? indent : ''
  if (instanceOfSList(sexp)) {
    const result = reindentSList(service, sexp, indented, firstIndent)
    return result
  }
  if (instanceOfSMembers(sexp)) {
    const result = [
      fIndent + reindentSExp(service, sexp.parent, indented + 1, false),
      '.',
      reindentSExp(service, sexp.child, indented + 1, false)
    ].join('')
    return result
  }
  return fIndent + sexp.toString()
}

export const reindentSProgram = (
  service: Service,
  sexp: SProgram,
  indented: number = 0,
  firstIndent: boolean = true
): string => {
  const indent = getIndent(service.indent, indented)
  const fIndent = firstIndent ? indent : ''
  return sexp
    .map(s => reindentSExp(service, s, indented, firstIndent))
    .join('\n')
}
