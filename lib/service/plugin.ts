import { Macro } from '../core/macros'
import { SExp, SProgram } from '../s-ast'
import { instanceOfSIdentifier } from '../s-ast/instance-of'
import { SList } from '../s-ast/s-list'
import { Environment } from '../transpiler/env'

type PluginDefinableType = 'macro' | 'boilerplate'

export abstract class PluginDefinable {
  type: PluginDefinableType = 'macro'
  name: string = ''
}

export class MacroPluginDefinable extends PluginDefinable {
  apply: Macro
  constructor(name: string, macro: Macro) {
    super()
    this.type = 'macro'
    this.apply = macro
  }
}

class InlineFunction {
  apply(env: Environment, slist: SList): SExp {
    const first = slist.getFirst()
    if (
      !slist.isList &&
      instanceOfSIdentifier(first) &&
      first.name === this.functionName
    ) {
      return this.func(env, ...slist.getLast())
    }
    return slist
  }
  functionName: string
  func: (env: Environment, ...args: SExp[]) => SList
  constructor(
    functionName: string,
    func: (env: Environment, ...args: SExp[]) => SList
  ) {
    this.functionName = functionName
    this.func = func
  }
}

export class InlineFunctionPluginDefinable extends MacroPluginDefinable {
  inlineFunction: InlineFunction
  constructor(
    name: string,
    functionName: string,
    inlineFunction: (env: Environment, ...args: SExp[]) => SList
  ) {
    super(name, (env, slist) => this.inlineFunction.apply(env, slist))
    this.inlineFunction = new InlineFunction(functionName, inlineFunction)
  }
}

export class BoilerplatePluginDefinable extends PluginDefinable {
  fileglobs: string[]
  apply: (filepath: string) => SProgram | SExp
  constructor(
    name: string,
    fileglobs: string[],
    creator: (filepath: string) => SProgram
  ) {
    super()
    this.type = 'boilerplate'
    this.apply = creator
    this.fileglobs = fileglobs
  }
}

export class Plugin {
  name: string
  definables: PluginDefinable[]
  constructor(name: string, definables: PluginDefinable[]) {
    this.name = name
    this.definables = definables
  }
}
