import generate from '@babel/generator'
import { controlStatements } from './core/control-statement'
import { parseSExp as _parseSExp } from './parser/parse'
import * as instanceOfs from './s-ast/instance-of'
import { listTypes } from './s-ast/list-type'
import { SIdentifier } from './s-ast/s-identifier'
import { SList } from './s-ast/s-list'
import { SString } from './s-ast/s-literal'
import { SMembers } from './s-ast/s-members'
import {
  SObjectPropertyIdentifier,
  SObjectPropertyNumeric,
  SObjectPropertyString
} from './s-ast/s-object-property'
import { Service as _Service } from './service/index'
import {
  BoilerplatePluginDefinable as _BoilerplatePluginDefinable,
  InlineFunctionPluginDefinable as _InlineFunctionPluginDefinable,
  MacroPluginDefinable as _MacroPluginDefinable,
  Plugin as _Plugin
} from './service/plugin'
import {
  reindentSExp as _reindentSExp,
  reindentSProgram as _reindentSProgram
} from './service/reindent'
import {
  Environment as _Environment,
  coreEnv as _coreEnv
} from './transpiler/env'
import {
  mapTranspileDefault as _mapTranspileDefault,
  transpileSExp as _transpileSExp
} from './transpiler/transpile'

// for user
export const transpileSExp = _transpileSExp
export const mapTranspileDefault = _mapTranspileDefault
export const Environment = _Environment
export type Environment = _Environment
export const Service = _Service
export type Service = _Service
export const parseSExp = _parseSExp
export const coreEnv = _coreEnv
export const reindentSExp = _reindentSExp
export const reindentSProgram = _reindentSProgram
export const Plugin = _Plugin
export const transpile = (code: string, env: Environment): string => {
  return mapTranspileDefault(parseSExp(code))
    .map(ast => generate(ast).code)
    .join('\n')
}

// for lib
export const _SIdentifier = SIdentifier
export const _listTypes = listTypes
export const _SList = SList
export const _SMembers = SMembers
export const _SString = SString
export const _SObjectPropertyIdentifier = SObjectPropertyIdentifier
export const _SObjectPropertyNumeric = SObjectPropertyNumeric
export const _SObjectPropertyString = SObjectPropertyString
export const MacroPluginDefinable = _MacroPluginDefinable
export const BoilerplatePluginDefinable = _BoilerplatePluginDefinable
export const InlineFunctionPluginDefinable = _InlineFunctionPluginDefinable
export const _controlStatements = controlStatements

export const _instanceOfs = instanceOfs
