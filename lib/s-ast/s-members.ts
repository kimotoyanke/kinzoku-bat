import { SExp } from '.'
import { SIdentifier } from './s-identifier'

export class SMembers extends SExp {
  constructor(parent: SExp, child: SIdentifier) {
    super()
    this.parent = parent
    this.child = child
    this.types.push('SMembers')
  }
  toString() {
    return this.parent.toString() + '.' + this.child.toString()
  }
  parent: SExp
  child: SIdentifier
}
