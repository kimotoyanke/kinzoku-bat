import { SExp } from '.'
import {
  instanceOfSObjectPropertyIdentifier,
  instanceOfSObjectPropertyNumeric,
  instanceOfSObjectPropertyString
} from './instance-of'

abstract class SAbstractObjectProperty<T> extends SExp {
  constructor(name: T) {
    super()
    this.type = 'object-property'
    this.name = name
  }
  name: T
  type: string
}

export class SObjectPropertyIdentifier extends SAbstractObjectProperty<string> {
  constructor(name: string) {
    super(name)
    this.types.push('SObjectPropertyIdentifier')
  }
  toString() {
    return this.name + ':'
  }
}
export class SObjectPropertyNumeric extends SAbstractObjectProperty<number> {
  constructor(name: number) {
    super(name)
    this.types.push('SObjectPropertyNumeric')
  }
  toString() {
    return this.name + ':'
  }
}
export class SObjectPropertyString extends SAbstractObjectProperty<string> {
  constructor(name: string) {
    super(name)
    this.types.push('SObjectPropertyString')
  }
  toString() {
    return '"' + this.name + '":'
  }
}

export type SObjectProperty =
  | SObjectPropertyIdentifier
  | SObjectPropertyNumeric
  | SObjectPropertyString

export const isSObjectProperty = (sexp: SExp) =>
  instanceOfSObjectPropertyIdentifier(sexp) ||
  instanceOfSObjectPropertyNumeric(sexp) ||
  instanceOfSObjectPropertyString(sexp)
