export abstract class SExp {
  types: string[] = ['SExp']
  abstract toString(): string
}

export type SProgram = SExp[]
