export const binaryOperators: Operator[] = [
  '+',
  '-',
  '/',
  '%',
  '*',
  '**',
  '&',
  '|',
  '>>',
  '>>>',
  '<<',
  '^'
]
export type Operator =
  | '+'
  | '-'
  | '/'
  | '%'
  | '*'
  | '**'
  | '&'
  | '|'
  | '>>'
  | '>>>'
  | '<<'
  | '^'
  | '=='
  | '==='
  | '!='
  | '!=='
  | 'in'
  | 'instanceof'
  | '>'
  | '<'
  | '>='
  | '<='

export const relationalOperators: Operator[] = [
  '==',
  '===',
  '!=',
  '!==',
  'in',
  'instanceof',
  '>',
  '<',
  '>=',
  '<='
]

export const operators = [...binaryOperators, ...relationalOperators]
