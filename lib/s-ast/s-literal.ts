import { SExp } from '.'

export abstract class SLiteral<
  T extends string | number | boolean
> extends SExp {
  constructor(val: T) {
    super()
    this.value = val
    this.types.push('SLiteral')
  }
  value: T
  abstract toString(): string
}

export class SString extends SLiteral<string> {
  constructor(val: string) {
    super(val)
    this.types.push('SString')
  }

  toString(): string {
    return '"' + this.value + '"'
  }
}

export class SNumeric extends SLiteral<number> {
  constructor(val: number) {
    super(val)
    this.types.push('SNumeric')
  }

  toString(): string {
    return this.value.toString()
  }
}

export class SBoolean extends SLiteral<boolean> {
  constructor(val: boolean) {
    super(val)
    this.types.push('SBoolean')
  }

  toString(): string {
    return this.value.toString()
  }
}
