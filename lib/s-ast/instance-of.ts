import { type } from 'os'
import { SExp } from '.'
import { SIdentifier } from './s-identifier'
import { SList } from './s-list'
import { SBoolean, SLiteral, SNumeric, SString } from './s-literal'
import { SMembers } from './s-members'
import {
  SObjectProperty,
  SObjectPropertyIdentifier,
  SObjectPropertyNumeric,
  SObjectPropertyString
} from './s-object-property'

export const instanceOfSExp = (instance: any): instance is SExp =>
  !!instance &&
  instance.types &&
  instance.types instanceof Array &&
  instance.types.includes('SExp')

export const instanceOfSIdentifier = (
  instance: SExp
): instance is SIdentifier => instance.types.includes('SIdentifier')

export const instanceOfSList = (instance: SExp): instance is SList =>
  instance.types && instance.types.includes('SList')

export const instanceOfSLiteral = (instance: SExp): instance is SLiteral<any> =>
  instance.types.includes('SLiteral')

export const instanceOfSNumeric = (instance: SExp): instance is SNumeric =>
  instance.types.includes('SNumeric')

export const instanceOfSString = (instance: SExp): instance is SString =>
  instance.types.includes('SString') &&
  // @ts-ignore
  instance['value'] &&
  // @ts-ignore
  typeof instance['value'] === 'string'

export const instanceOfSBoolean = (instance: SExp): instance is SBoolean =>
  instance.types.includes('SBoolean')

export const instanceOfSMembers = (instance: SExp): instance is SMembers =>
  instance.types.includes('SMembers')

export const instanceOfSObjectPropertyIdentifier = (
  instance: SExp
): instance is SObjectPropertyIdentifier =>
  instance.types.includes('SObjectPropertyIdentifier')

export const instanceOfSObjectPropertyNumeric = (
  instance: SExp
): instance is SObjectPropertyNumeric =>
  instance.types.includes('SObjectPropertyNumeric') &&
  // @ts-ignore
  instance['name'] &&
  // @ts-ignore
  typeof instance['name'] === 'number'

export const instanceOfSObjectPropertyString = (
  instance: SExp
): instance is SObjectPropertyString =>
  instance.types.includes('SObjectPropertyString') &&
  // @ts-ignore
  instance['name'] &&
  // @ts-ignore
  typeof instance['name'] === 'string'

export const instanceOfSObjectProperty = (
  instance: SExp
): instance is SObjectProperty =>
  (instanceOfSObjectPropertyIdentifier(instance) ||
    instanceOfSObjectPropertyNumeric(instance) ||
    instanceOfSObjectPropertyString(instance)) &&
  !!instance['name']
