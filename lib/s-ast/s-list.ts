import { SExp } from '.'
import { instanceOfSObjectProperty } from './instance-of'
import { SBracket, SListType, bracketListTypes } from './list-type'

export type SStatementOption = number | undefined
export class SList extends SExp {
  constructor(
    expressions: SExp[],
    type: SListType = bracketListTypes.round,
    isList = false
  ) {
    super()
    this.listType = type
    this.expressions = expressions
    this.isList = isList
    this.types.push('SList')
  }
  listType: SListType
  expressions: SExp[]
  statementOption: SStatementOption
  getFirst(): SExp {
    return this.expressions[0]
  }
  getLast(): SExp[] {
    return this.expressions.slice(1)
  }
  toString() {
    return '(' + this.isList
      ? ' '
      : '' + this.expressions.map(sexp => sexp.toString()).join(' ') + ')'
  }

  get defaultBracket(): SBracket {
    if (this.expressions.some(exp => instanceOfSObjectProperty(exp))) {
      return bracketListTypes['curly']
    }
    if (this.isList) {
      return bracketListTypes['square']
    }
    return bracketListTypes['round']
  }
  isList: boolean
}
export type SExcutableList = SList & { isList: false }
export type SUnexcutableList = SList & { isList: true }
