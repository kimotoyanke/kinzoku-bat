type BracketName = 'round' | 'square' | 'curly'
type SeparatorName = 'semi'
type XhaniName = 'xhani-element'
type ReaderMacroName = 'reader-macro'
type ListTypeName = BracketName | SeparatorName | XhaniName | ReaderMacroName

export abstract class SListType {
  name: ListTypeName = 'round'
}

export const isSListType = (o: any): o is SListType => {
  return o && typeof o.name === 'string'
}

export abstract class SBracket extends SListType {
  start: string = '('
  end: string = ')'
}

export const isSBracket = (o: any): o is SBracket => {
  return (
    o &&
    typeof o.start === 'string' &&
    typeof o.end === 'string' &&
    isSListType(o)
  )
}

export abstract class SListSeparator extends SListType {
  separator: string = ';'
}

export const instanceOfSListSeparator = (o: any): o is SListSeparator => {
  return o && typeof o.separator === 'string' && isSListType(o)
}

class SListTypeXhani extends SListType {
  name: XhaniName = 'xhani-element'
}

class SListTypeRound extends SBracket {
  name: BracketName = 'round'
  start = '('
  end = ')'
}
class SListTypeSquare extends SBracket {
  name: BracketName = 'square'
  start = '['
  end = ']'
}
class SListTypeCurly extends SBracket {
  name: BracketName = 'curly'
  start = '{'
  end = '}'
}

class SListTypeSemi extends SListSeparator {
  name: SeparatorName = 'semi'
  separator = ';'
}

type ListTypesObject<T extends SListType, N extends ListTypeName> = {
  [name in N]: T
}
function listTypesObject<T extends SListType>(
  ...type: T[]
): ListTypesObject<T, ListTypeName> {
  return type.reduce(
    (p, c) => {
      return { ...p, [c.name]: c }
    },
    {} as ListTypesObject<T, ListTypeName>
  )
}
export const bracketListTypes = listTypesObject<SBracket>(
  new SListTypeRound(),
  new SListTypeSquare(),
  new SListTypeCurly()
) as {
  round: SListTypeRound
  square: SListTypeSquare
  curly: SListTypeCurly
}
export const separatorListTypes = listTypesObject<SListSeparator>(
  new SListTypeSemi()
) as ListTypesObject<SListTypeSemi, SeparatorName>
export const xhaniListTypes = listTypesObject<SListTypeXhani>(
  new SListTypeXhani()
) as ListTypesObject<SListTypeXhani, XhaniName>

export const listTypes = {
  ...bracketListTypes,
  ...separatorListTypes,
  ...xhaniListTypes
} as ListTypesObject<SListType, BracketName | SeparatorName | XhaniName>
