import { SExp } from '.'

export class SIdentifier extends SExp {
  constructor(name: string) {
    super()
    this.name = name
    this.types.push('SIdentifier')
  }
  toString() {
    return this.name
  }
  name: string
}
