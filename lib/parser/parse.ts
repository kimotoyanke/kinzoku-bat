import * as P from 'parsimmon'
import { SProgram } from '../s-ast'
import { memberParser } from './members'

export const elementParser = P.lazy(() => memberParser)

export function parseSExp(str: string): SProgram {
  return elementParser
    .wrap(P.optWhitespace, P.optWhitespace)
    .many()
    .tryParse(str)
}
