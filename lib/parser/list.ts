import * as P from 'parsimmon'
import { curry, values } from 'ramda'
import { SExp } from '../s-ast'
import { instanceOfSExp } from '../s-ast/instance-of'
import {
  SListSeparator,
  bracketListTypes,
  instanceOfSListSeparator,
  separatorListTypes
} from '../s-ast/list-type'
import { SList } from '../s-ast/s-list'
import { propertyNameParser } from './object'
import { elementParser } from './parse'
import { readerMacroParser } from './reader-macro'
import { xhaniParser } from './xhani'

const separatorParser: P.Parser<SListSeparator> = P.alt(
  ...values(separatorListTypes).map(b => P.string(b.separator).map(_ => b))
)

const separate = curry(
  (isList: boolean, elements: (SListSeparator | SExp)[]) => {
    let buf: SExp[] = []
    let result: SExp[] = []
    for (let element of elements) {
      if (instanceOfSListSeparator(element)) {
        result.push(new SList(buf, element, isList))
        buf = []
      }
      if (instanceOfSExp(element)) {
        buf.push(element)
      }
    }
    return [...result, ...buf]
  }
)

const executableListContentParser = P.lazy(() =>
  P.seqMap(
    P.seqMap(
      P.alt(propertyNameParser, elementParser).skip(P.optWhitespace),
      P.alt(separatorParser, elementParser)
        .trim(P.optWhitespace)
        .many()
        .map(separate(false)),
      (first, s: SExp[]) => {
        return [first, ...s]
      }
    ),
    (s: SExp[]) => ({ elements: s, isList: false })
  )
)

const unexecutableListContentParser = P.lazy(() =>
  P.seqMap(
    P.whitespace,
    P.alt(separatorParser, propertyNameParser, elementParser)
      .skip(P.optWhitespace)
      .many()
      .map(separate(true)),
    (_, s: SExp[]) => ({ elements: s, isList: true })
  )
)
export const bracketListParser = P.alt(
  ...Object.values(bracketListTypes).map(b =>
    P.lazy(() =>
      P.alt(executableListContentParser, unexecutableListContentParser)
        .wrap(P.string(b.start), P.string(b.end))
        .map(({ elements, isList }) => new SList(elements, b, isList))
    )
  )
)

export const listParser = P.lazy(
  () =>
    P.alt(xhaniParser, bracketListParser, readerMacroParser) as P.Parser<SList>
)
