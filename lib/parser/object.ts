import * as P from 'parsimmon'
import {
  instanceOfSIdentifier,
  instanceOfSNumeric,
  instanceOfSString
} from '../s-ast/instance-of'
import {
  SObjectPropertyIdentifier,
  SObjectPropertyNumeric,
  SObjectPropertyString
} from '../s-ast/s-object-property'
import { stringParser } from './literal'
import { numericParser } from './numeric'
import { identifierParser } from './symbol'

export const propertyNameParser = P.alt(
  identifierParser,
  numericParser,
  stringParser
)
  .skip(P.string(':'))
  .map(result => {
    if (instanceOfSIdentifier(result)) {
      return new SObjectPropertyIdentifier(result.name)
    }
    if (instanceOfSNumeric(result)) {
      return new SObjectPropertyNumeric(result.value)
    }
    if (instanceOfSString(result)) {
      return new SObjectPropertyString(result.value)
    }
  })
