import * as P from 'parsimmon'
import { SBoolean, SNumeric, SString } from '../s-ast/s-literal'
import { hexDigit, numericParser } from './numeric'
import { unicodeEscapeSequence } from './unicode'

const hexEscapeSequence = P.string('x')
  .then(hexDigit.times(2))
  .tie()
  .map(digits => {
    return String.fromCharCode(parseInt(digits, 16))
  })

const escapeSequenceLang = P.createLanguage({
  CharacterEscapeSequence: r =>
    P.alt(
      P.string("'"),
      P.string('"'),
      P.string('\\'),
      P.string('b'),
      P.string('f'),
      P.string('n'),
      P.string('r'),
      P.string('t'),
      P.string('v'),
      r.nonEscapeCharacter
    ),
  EscapeSequence: r =>
    P.alt(
      r.characterEscapeSequence,
      P.string('0')
        .notFollowedBy(P.digit)
        .map(() => '\0'),
      hexEscapeSequence,
      unicodeEscapeSequence
    ),
  NonEscapeCharacter: r =>
    P.regexp(/./).notFollowedBy(P.alt(r.escapeSequence, P.newline, P.cr))
})

const escapeSequence: P.Parser<string> = escapeSequenceLang.EscapeSequence

const doubleStringCharacter = P.alt(
  P.noneOf('"\\\n\r'),
  P.string('\\')
    .then(escapeSequence)
    .map(e => '\\' + e)
)

export const stringParser = P.string('"')
  .then(doubleStringCharacter.many())
  .skip(P.string('"'))
  .tie()
  .map(val => new SString(val))
export const booleanParser = P.alt(
  P.string('true').map(() => new SBoolean(true)),
  P.string('false').map(() => new SBoolean(false))
)

export const literalParser: P.Parser<SString | SBoolean | SNumeric> = P.alt(
  stringParser,
  booleanParser,
  numericParser
)
