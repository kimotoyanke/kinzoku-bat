import * as P from 'parsimmon'
import { SNumeric } from '../s-ast/s-literal'
import { concatArray } from './parser-helper'

export const hexDigit = P.regexp(/[0-9a-f]/i)
const hexInteger = P.string('0x')
  .then(hexDigit.many())
  .tie()
  .map(hex => new SNumeric(parseInt(hex, 16)))

const nonZeroDigit = P.regexp(/[1-9]/)
const decimalInteger = P.alt(
  P.string('0'),
  P.seq(nonZeroDigit, P.digits).map(concatArray)
)
const sign = P.regexp(/[+-]?/)
const signedInteger = P.seq(sign, P.digit.atLeast(1).map(concatArray)).map(
  concatArray
)
const exponentIndicator = P.regexp(/e/i)
const exponentPart = P.seq(exponentIndicator, signedInteger).map(concatArray)
const decimal = P.seq(
  sign,
  P.alt(
    P.seq(
      decimalInteger,
      P.string('.'),
      P.digits,
      exponentPart.atMost(1).tie()
    ),
    P.seq(
      P.string('.'),
      P.digit.atLeast(1).tie(),
      exponentPart.atMost(1).tie()
    ),
    P.seq(decimalInteger, exponentPart.atMost(1).tie())
  ).tie()
)
  .tie()
  .map(num => {
    return new SNumeric(Number(num))
  })

const numeric = P.alt(hexInteger, decimal)
export const numericParser = numeric.desc('number')
