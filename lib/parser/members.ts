import * as P from 'parsimmon'
import { SExp } from '../s-ast'
import { SIdentifier } from '../s-ast/s-identifier'
import { SMembers } from '../s-ast/s-members'
import { listParser } from './list'
import { literalParser } from './literal'
import { readerMacroParser } from './reader-macro'
import { identifierParser } from './symbol'

const elementsArrayToMembers = (elements: SIdentifier[]): SExp => {
  if (elements.length === 1) {
    return elements[0]
  } else {
    let newIdentifiers = [...elements]
    const last = newIdentifiers.pop() as SIdentifier
    return new SMembers(elementsArrayToMembers(newIdentifiers), last)
  }
}

export const memberParser: P.Parser<SExp> = P.lazy(() =>
  P.seqMap(
    P.alt(listParser, identifierParser, literalParser),
    P.string('.')
      .then(P.alt(identifierParser, literalParser))
      .many(),
    (parent, elements) => {
      return elementsArrayToMembers([parent, ...elements])
    }
  ).desc('member')
)
