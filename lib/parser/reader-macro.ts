import * as P from 'parsimmon'
import { last, toPairs } from 'ramda'
import { SExp } from '../s-ast'
import { SListType, bracketListTypes, isSListType } from '../s-ast/list-type'
import { SIdentifier } from '../s-ast/s-identifier'
import { SList } from '../s-ast/s-list'
import { bracketListParser } from './list'
import { createSIdentifier, symbolParser } from './symbol'

export class SListTypeReaderMacro extends SListType {
  char: string
  funcName: string
  name: 'reader-macro' = 'reader-macro'
  reduce(args: SExp[]): SExp | null {
    return last(args) || null
  }
  constructor(char: string, funcName: string) {
    super()
    this.char = char
    this.funcName = funcName
  }
}
export class QuoteSListTypeReaderMacro extends SListTypeReaderMacro {
  char!: string
  funcName!: string
  name: 'reader-macro' = 'reader-macro'
  reduce(args: SExp[]): SExp {
    if (args.length === 1) {
      return args[0]
    }
    return new SList(
      [new SIdentifier('do'), ...args],
      bracketListTypes.curly,
      false
    )
  }
  constructor(char: string, funcName: string) {
    super(char, funcName)
  }
}

export const readerMacros = Object.freeze([
  new QuoteSListTypeReaderMacro("'", 'quote'),
  new QuoteSListTypeReaderMacro('`', 'quasiquote'),
  new SListTypeReaderMacro(',', 'unquote'),
  new SListTypeReaderMacro('...,', 'unquote-spread'),
  new SListTypeReaderMacro('...', 'spread')
])

export const isSListTypeReaderMacro = (o: any): o is SListTypeReaderMacro => {
  return (
    o &&
    typeof o.char === 'string' &&
    typeof o.funcName === 'string' &&
    o.name === 'reader-macro' &&
    isSListType(o)
  )
}

const readerMacroListTypes: { [char: string]: SListTypeReaderMacro } = {}
const getReaderMacroListType = (
  char: string,
  funcName: string
): SListTypeReaderMacro => {
  if (!readerMacroListTypes[char]) {
    readerMacroListTypes[char] = new SListTypeReaderMacro(char, funcName)
  }
  return readerMacroListTypes[char]
}

export const readerMacroParser = P.lazy(() =>
  P.alt(
    ...readerMacros.map(({ char, funcName }) => {
      return P.string(char)
        .then(P.alt(bracketListParser, symbolParser))
        .map(
          l =>
            new SList(
              [createSIdentifier(funcName), l],
              getReaderMacroListType(char, funcName)
            )
        )
    })
  )
)
