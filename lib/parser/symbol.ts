import * as P from 'parsimmon'
import { operators } from '../s-ast/operator'
import { SIdentifier } from '../s-ast/s-identifier'
import { SLiteral } from '../s-ast/s-literal'
import { SMembers } from '../s-ast/s-members'
import { literalParser } from './literal'
import { memberParser } from './members'
import { Ll, Lu, unicodeEscapeSequence, unicodeLetter } from './unicode'

const exchangeCase = (c: string): string => {
  if (c.toLocaleLowerCase() !== c) {
    return c.toLocaleLowerCase()
  } else {
    return c.toLocaleUpperCase()
  }
}

const hyphenCapital = P.string('-')
  .then(P.alt(Lu, Ll))
  .map(exchangeCase)

const operator = P.alt(...operators.map(P.string)).skip(P.whitespace)
export const identifierStart = P.alt(
  unicodeLetter,
  hyphenCapital,
  P.oneOf('$_'),
  P.string('\\').then(unicodeEscapeSequence)
)
export const identifierPart = P.alt(identifierStart, P.digit)

const identifierName = P.alt(
  P.seq(identifierStart, identifierPart.many().tie()).tie(),
  operator
).desc('Identifier')

export const identifierParser = identifierName.map(
  name => new SIdentifier(name)
)

export const createSIdentifierName = (str: string) => {
  return identifierName.tryParse(str)
}
export const createSIdentifier = (str: string) => {
  if (str === '') {
    return new SIdentifier(str)
  }
  return identifierParser.tryParse(str)
}

export const symbolParser: P.Parser<
  SMembers | SIdentifier | SLiteral<any>
> = P.alt(identifierParser, literalParser)
