import * as P from 'parsimmon'
import { instanceOfSIdentifier, instanceOfSString } from '../s-ast/instance-of'
import { SListType, xhaniListTypes } from '../s-ast/list-type'
import { SIdentifier } from '../s-ast/s-identifier'
import { SList } from '../s-ast/s-list'
import { SString } from '../s-ast/s-literal'
import { SObjectPropertyString } from '../s-ast/s-object-property'
import { stringParser } from './literal'
import { identifierParser } from './symbol'
import { unicodeEscapeSequence, unicodeLetter } from './unicode'

type XhElement = {
  name: SIdentifier | SString
  attributes: XhAttribute[]
  children: (SList | SString)[]
}
type XhAttribute = {
  name: string
  value: SString
}

const tagnameEquals = (a: SIdentifier | SString, b: SIdentifier | SString) => {
  if (instanceOfSIdentifier(a)) {
    return instanceOfSIdentifier(b) && a.name === b.name
  }
  if (instanceOfSString(a)) {
    return instanceOfSString(b) && a.value === b.value
  }
  return false
}

const xhElementName = identifierParser.map(identifier => {
  if (identifier.name.charAt(0).match(/[a-z]/)) {
    return new SString(identifier.name)
  }
  return identifier
})

const xhIdentifierStart = P.alt(
  unicodeLetter,
  P.oneOf('$_'),
  P.string('\\').then(unicodeEscapeSequence),
  P.oneOf('@-:.')
)
const xhIdentifierPart = P.alt(xhIdentifierStart, P.digit)

const xhIdentifier = P.seq(
  xhIdentifierStart,
  xhIdentifierPart.many().tie()
).tie()

const xhAttribute: P.Parser<XhAttribute> = P.seqMap(
  xhIdentifier,
  P.optWhitespace,
  P.string('='),
  P.optWhitespace,
  stringParser,
  (name, _0, _1, _2, value): XhAttribute => {
    return { name, value }
  }
)

const xhAttributes = xhAttribute.wrap(P.optWhitespace, P.optWhitespace).many()

const xhSelfClosingElement = P.seqMap(
  P.string('<'),
  P.optWhitespace,
  xhElementName,
  P.optWhitespace,
  xhAttributes,
  P.optWhitespace,
  P.string('/>'),
  (_0, _1, name, _2, attributes, _3, _4): XhElement => {
    return { name, attributes, children: [] }
  }
)

const xhOpeningElement = P.seqMap(
  P.string('<'),
  P.optWhitespace,
  xhElementName,
  P.optWhitespace,
  xhAttributes,
  P.optWhitespace,
  P.string('>'),
  (_0, _1, name, _2, attributes, _3, _4): XhElement => {
    return { name, attributes, children: [] }
  }
)

const xhClosingElement = P.seqMap(
  P.string('</'),
  P.optWhitespace,
  xhElementName,
  P.optWhitespace,
  xhAttributes,
  P.optWhitespace,
  P.string('>'),
  (_0, _1, name, _2, attributes, _3, _4): XhElement => {
    return { name, attributes, children: [] }
  }
)

const xhText = P.regex(/[^{<]+/)
  .desc('X-Hani Text')
  .map(str => new SString(str))

export const createObjectLiteral = (obj: XhAttribute[], listType: SListType) =>
  new SList(
    obj.map(({ name, value }) => {
      return new SList(
        [new SObjectPropertyString(name), value],
        listType,
        false
      )
    }),
    listType,
    true
  )

const lang = P.createLanguage({
  xhChildren: r => P.alt(xhText, r.xhElement).many(),
  xhElement: r =>
    P.alt(
      xhSelfClosingElement,
      P.seqMap(
        xhOpeningElement,
        P.optWhitespace,
        r.xhChildren,
        P.optWhitespace,
        xhClosingElement,
        (opening: XhElement, _0, children, _1, closing: XhElement) => {
          if (!tagnameEquals(opening.name, closing.name)) {
            throw new Error(
              `Tag names don\'t match: opening tag:[${opening}], closing tag:[${closing}]`
            )
          }
          return {
            name: opening.name,
            attributes: opening.attributes,
            children
          }
        }
      )
    ).map(element => {
      const hSymbol = new SIdentifier('h')
      return new SList(
        [
          hSymbol,
          element.name,
          new SList(
            element.attributes.map(attr => {
              return new SList(
                [new SObjectPropertyString(attr.name), attr.value],
                xhaniListTypes['xhani-element'],
                false
              )
            }),
            xhaniListTypes['xhani-element'],
            true
          ),
          ...element.children
        ],
        xhaniListTypes['xhani-element']
      )
    })
})
export const xhaniParser = lang.xhElement
