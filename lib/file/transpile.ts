import generate from '@babel/generator'
import { mkdirp, readFile, writeFile } from 'fs-extra'
import { basename, resolve } from 'path'
import { curry } from 'ramda'
import { parseSExp } from '../parser/parse'
import { Environment } from '../transpiler/env'
import { transpileSExp } from '../transpiler/transpile'

export const transpileFile = curry(
  async (env: Environment, filePath: string, outputFilePath: string) => {
    const file: Buffer = await readFile(filePath)
    const strCode = file.toString()
    const jsCode = parseSExp(strCode)
      .map(ast => generate(transpileSExp(env, ast)).code)
      .join('\n')
    await writeFile(outputFilePath, jsCode)
    return outputFilePath
  }
)

export const transpileFileAsCache = async (
  env: Environment,
  filePath: string,
  cacheDirName = '.kb-hotcake'
) => {
  const cacheDirPath = resolve(filePath, '..', cacheDirName)
  await mkdirp(cacheDirPath)

  const cachePath = resolve(cacheDirPath, basename(filePath))
  return transpileFile(env, filePath, cachePath)
}
