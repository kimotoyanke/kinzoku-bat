import * as t from '@babel/types'
import macros, { Macro } from '../core/macros'
import transformations, { Transformation } from '../core/transformations'
import { createSIdentifier, createSIdentifierName } from '../parser/symbol'
import { bracketListTypes, separatorListTypes } from '../s-ast/list-type'
import { SList } from '../s-ast/s-list'
import { SString } from '../s-ast/s-literal'
import { transpileSExp } from './transpile'

class KbImportsSet {
  set: Set<string>
  add(name: string) {
    this.set.add(createSIdentifierName(name))
  }
  values(): string[] {
    return Array.from(this.set.values())
  }
  constructor(s: string[] = []) {
    this.set = new Set(s)
  }
  size(): number {
    return this.set.size
  }
}
export class Environment {
  macros: {
    [name: string]: Macro
  } = {}
  transformations: {
    [name: string]: Transformation
  } = {}
  kbImports: KbImportsSet
  constructor() {
    this.kbImports = new KbImportsSet()
  }
}

export class CoreEnvironment extends Environment {
  constructor() {
    super()
    this.macros = { ...macros }
    this.transformations = { ...transformations }
  }
}
export const coreEnv: Environment = new CoreEnvironment()

export const importFromEnvironments = (env: Environment): t.Node[] => {
  if (env.kbImports.size() > 0) {
    return [
      transpileSExp(
        env,
        new SList(
          [
            createSIdentifier('import-from'),
            new SList(
              env.kbImports.values().map(moduleName => {
                return createSIdentifier(moduleName)
              }),
              separatorListTypes.semi,
              true
            ),
            new SString('kinzoku-bat')
          ],
          bracketListTypes.round,
          false
        )
      )
    ]
  }
  return []
}
