import * as t from '@babel/types'
import { CurriedFunction2, curry } from 'ramda'
import { SExp } from '../s-ast'
import {
  instanceOfSBoolean,
  instanceOfSIdentifier,
  instanceOfSList,
  instanceOfSMembers,
  instanceOfSNumeric,
  instanceOfSObjectProperty,
  instanceOfSObjectPropertyIdentifier,
  instanceOfSObjectPropertyNumeric,
  instanceOfSObjectPropertyString,
  instanceOfSString
} from '../s-ast/instance-of'
import { SExcutableList, SList, SUnexcutableList } from '../s-ast/s-list'
import { CoreEnvironment, Environment, importFromEnvironments } from './env'

export const mapTranspile = curry(
  (env: Environment, sasts: SExp[]): t.Node[] => {
    return sasts.map(transpileSExp(env))
  }
)

const createExpressionError = (sast: SExp) =>
  new Error(`${JSON.stringify(sast)} is not a expression`)

const createElementError = (sast: SExp, expression: any) =>
  new Error(
    `${JSON.stringify(sast)} is not a element: ${JSON.stringify(expression)}`
  )

export const transpileAsExpression = curry(
  (env: Environment, sast: SExp): t.Expression => {
    const expression = transpileSExp(env, sast)
    if (!t.isExpression(expression) && !t.isLiteral(expression)) {
      throw createExpressionError(sast)
    }
    return expression as t.Expression
  }
)

const mapTranspileAsElement = curry((env: Environment, sasts: SExp[]) =>
  sasts.map(transpileAsElement(env))
)

export const transpileAsElement = curry(
  (env: Environment, sast: SExp): t.Expression => {
    const expression = transpileSExp(env, sast)
    if (
      !t.isExpression(expression) &&
      !t.isLiteral(expression) &&
      !t.isSpreadElement(expression)
    ) {
      throw createElementError(sast, expression)
    }
    return expression as t.Expression
  }
)

export const transpileAs = curry(
  <T extends t.Node>(
    isT: ((n: t.Node) => boolean),
    typeName: string,
    env: Environment,
    sast: SExp
  ): T => {
    const t = transpileSExp(env, sast)
    if (!isT(t)) {
      throw new Error(`${sast.toString()} is not a ${typeName}`)
    }
    return t as T
  }
)

export const transpileAsVariableDeclaration = transpileAs(
  t.isVariableDeclaration,
  'VariableDeclaration'
) as CurriedFunction2<{}, {}, t.VariableDeclaration>
export const transpileAsDeclaration = transpileAs(
  t.isDeclaration,
  'Declaration'
) as CurriedFunction2<{}, {}, t.Declaration>

export const transpileAsStringLiteral = transpileAs(
  t.isStringLiteral,
  'StringLiteral'
) as CurriedFunction2<{}, {}, t.StringLiteral>
export const transpileAsIdentifier = transpileAs(
  t.isIdentifier,
  'Identifier'
) as CurriedFunction2<{}, {}, t.Identifier>
export const transpileAsLVal = transpileAs(
  t.isLVal,
  'LVal'
) as CurriedFunction2<{}, {}, t.Identifier>

const transpileExcutableList = curry(
  (env: Environment, sast: SExcutableList): t.Node => {
    const first = sast.getFirst()
    const last = sast.getLast()
    if (instanceOfSIdentifier(first)) {
      const m = env.macros[first.name]
      if (m) {
        const macroApplied = m(env, sast)
        return transpileSExp(env, macroApplied)
      }
      const t = env.transformations[first.name]
      if (t) {
        const args = sast.getLast()
        return t(env, ...args)
      }
    }
    return t.callExpression(
      transpileAsExpression(env, first),
      last.length === 0 ? [] : mapTranspileAsElement(env, last)
    )
  }
)

const transpileUnexcutableList = curry(
  (env: Environment, sast: SUnexcutableList) => {
    if (
      sast.expressions.every(
        sexp =>
          (instanceOfSList(sexp) &&
            instanceOfSObjectProperty(sexp.getFirst())) ||
          instanceOfSObjectPropertyIdentifier(sexp)
      )
    ) {
      return t.objectExpression(sast.expressions
        .map(sexp => {
          if (instanceOfSList(sexp)) {
            const key = sexp.getFirst()
            const value = sexp.expressions[1]

            if (instanceOfSObjectPropertyIdentifier(key)) {
              return t.objectProperty(
                t.identifier(key.name),
                transpileAsExpression(env, value)
              )
            }

            if (instanceOfSObjectPropertyNumeric(key)) {
              return t.objectProperty(
                t.numericLiteral(key.name),
                transpileAsExpression(env, value)
              )
            }
            if (instanceOfSObjectPropertyString(key)) {
              return t.objectProperty(
                t.stringLiteral(key.name),
                transpileAsExpression(env, value)
              )
            }
          } else if (instanceOfSObjectPropertyIdentifier(sexp)) {
            const key = sexp
            return t.objectProperty(
              t.stringLiteral(key.name),
              t.identifier(key.name)
            )
          }
          return null
        })
        .filter(s => s !== null) as (t.ObjectProperty)[])
    }

    return t.arrayExpression(mapTranspileAsElement(env, sast.expressions))
  }
)

export const transpileAsStatement = curry(
  (env: Environment, sast: SExp): t.Statement => {
    const statement = transpileSExp(env, sast)
    if (t.isExpression(statement)) {
      return t.expressionStatement(statement as t.Expression)
    }
    return statement as t.Statement
  }
)

export const transpileSExp = curry(
  (env: Environment, sast: SExp): t.Node => {
    if (instanceOfSIdentifier(sast)) {
      return t.identifier(sast.name)
    }

    if (instanceOfSMembers(sast)) {
      return t.memberExpression(
        transpileAsExpression(env, sast.parent),
        transpileAsExpression(env, sast.child)
      )
    }

    if (instanceOfSList(sast)) {
      if (sast.isList) {
        return transpileUnexcutableList(env, sast as SUnexcutableList)
      } else {
        return transpileExcutableList(env, sast as SExcutableList)
      }
    }

    if (instanceOfSString(sast)) {
      return t.stringLiteral(sast.value)
    }

    if (instanceOfSBoolean(sast)) {
      return t.booleanLiteral(sast.value)
    }

    if (instanceOfSNumeric(sast)) {
      return t.numericLiteral(sast.value)
    }
    return t.identifier('Error')
  }
)

export const mapTranspileDefault = (sasts: SExp[]) => {
  const env = new CoreEnvironment()
  const translated = sasts.map(transpileSExp(env))
  return [...importFromEnvironments(env), ...translated]
}

export const transpileDefault = (sast: SExp) => {
  const env = new CoreEnvironment()
  return transpileSExp(env, sast)
}
