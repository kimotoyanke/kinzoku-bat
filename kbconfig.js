// @ts-check
const { Service } = require('./dist/index')
const core = require('./packages/plugin-core/')

const config = new Service([core.default])

module.exports = config
